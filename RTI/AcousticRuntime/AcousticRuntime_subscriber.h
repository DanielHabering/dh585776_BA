#include <stdlib.h>
#include <stdio.h>
#include <ndds/ndds_cpp.h>
#include "AcousticRuntimeSupport.h"
#include "AcousticRuntime.h"

typedef void (*AcousticRuntimeCallback)(gazebo_AcousticRuntime* instance);
int AcousticRuntimeSubscriber_init(int domainId, char* topicName, AcousticRuntimeCallback cbpointer);