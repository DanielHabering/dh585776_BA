#include <stdio.h>
#include <stdlib.h>
#include "AcousticRuntime.h"
#include "AcousticRuntimeSupport.h"
#include <ndds/ndds_cpp.h>

int AcousticRuntime_write(gazebo_AcousticRuntime* instance);
int AcousticRuntimePublisher_init(int domainId, char* topicName);