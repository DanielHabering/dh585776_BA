#include <stdio.h>
#include <stdlib.h>
#include <ndds/ndds_cpp.h>
#include "Return.h"
#include "ReturnSupport.h"

typedef void (*ReturnCallback) (range_Return* instance);
int ReturnSubscriber_init(int domainId, char* topicName, ReturnCallback returnPointer);
