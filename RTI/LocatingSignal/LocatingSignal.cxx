

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from LocatingSignal.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif        

#ifndef cdr_type_h
#include "cdr/cdr_type.h"
#endif    

#ifndef osapi_heap_h
#include "osapi/osapi_heap.h" 
#endif
#else
#include "ndds_standalone_type.h"
#endif

#include "LocatingSignal.h"

/* ========================================================================= */
const char *Acoustic_ParticleTYPENAME = "Acoustic::Particle";

DDS_TypeCode* Acoustic_Particle_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode_Member Acoustic_Particle_g_tc_members[4]=
    {

        {
            (char *)"x",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"y",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"z",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"w",/* Member name */
            {
                3,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode Acoustic_Particle_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"Acoustic::Particle", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            4, /* Number of members */
            Acoustic_Particle_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for Acoustic_Particle*/

    if (is_initialized) {
        return &Acoustic_Particle_g_tc;
    }

    Acoustic_Particle_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_double;

    Acoustic_Particle_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_double;

    Acoustic_Particle_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_double;

    Acoustic_Particle_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_double;

    is_initialized = RTI_TRUE;

    return &Acoustic_Particle_g_tc;
}

RTIBool Acoustic_Particle_initialize(
    Acoustic_Particle* sample) {
    return Acoustic_Particle_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool Acoustic_Particle_initialize_ex(
    Acoustic_Particle* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return Acoustic_Particle_initialize_w_params(
        sample,&allocParams);

}

RTIBool Acoustic_Particle_initialize_w_params(
    Acoustic_Particle* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (allocParams) {} /* To avoid warnings */

    if (!RTICdrType_initDouble(&sample->x)) {
        return RTI_FALSE;
    }     

    if (!RTICdrType_initDouble(&sample->y)) {
        return RTI_FALSE;
    }     

    if (!RTICdrType_initDouble(&sample->z)) {
        return RTI_FALSE;
    }     

    if (!RTICdrType_initDouble(&sample->w)) {
        return RTI_FALSE;
    }     

    return RTI_TRUE;
}

void Acoustic_Particle_finalize(
    Acoustic_Particle* sample)
{

    Acoustic_Particle_finalize_ex(sample,RTI_TRUE);
}

void Acoustic_Particle_finalize_ex(
    Acoustic_Particle* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    Acoustic_Particle_finalize_w_params(
        sample,&deallocParams);
}

void Acoustic_Particle_finalize_w_params(
    Acoustic_Particle* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }
    if (deallocParams) {} /* To avoid warnings */

}

void Acoustic_Particle_finalize_optional_members(
    Acoustic_Particle* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool Acoustic_Particle_copy(
    Acoustic_Particle* dst,
    const Acoustic_Particle* src)
{

    if (!RTICdrType_copyDouble (
        &dst->x, &src->x)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyDouble (
        &dst->y, &src->y)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyDouble (
        &dst->z, &src->z)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyDouble (
        &dst->w, &src->w)) { 
        return RTI_FALSE;
    }

    return RTI_TRUE;
}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'Acoustic_Particle' sequence class.
*/
#define T Acoustic_Particle
#define TSeq Acoustic_ParticleSeq
#define T_initialize_w_params Acoustic_Particle_initialize_w_params
#define T_finalize_w_params   Acoustic_Particle_finalize_w_params
#define T_copy       Acoustic_Particle_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params
#undef T_initialize_w_params
#undef TSeq
#undef T

/* ========================================================================= */
const char *Acoustic_LocatingSignalTYPENAME = "Acoustic::LocatingSignal";

DDS_TypeCode* Acoustic_LocatingSignal_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode Acoustic_LocatingSignal_g_tc_sender_string = DDS_INITIALIZE_STRING_TYPECODE((64));
    static DDS_TypeCode Acoustic_LocatingSignal_g_tc_receiver_string = DDS_INITIALIZE_STRING_TYPECODE((64));
    static DDS_TypeCode Acoustic_LocatingSignal_g_tc_particleSet_sequence = DDS_INITIALIZE_SEQUENCE_TYPECODE((100),NULL);
    static DDS_TypeCode_Member Acoustic_LocatingSignal_g_tc_members[6]=
    {

        {
            (char *)"sender",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"receiver",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"timestamp",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"size",/* Member name */
            {
                3,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"particleSet",/* Member name */
            {
                4,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"error",/* Member name */
            {
                5,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode Acoustic_LocatingSignal_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"Acoustic::LocatingSignal", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            6, /* Number of members */
            Acoustic_LocatingSignal_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for Acoustic_LocatingSignal*/

    if (is_initialized) {
        return &Acoustic_LocatingSignal_g_tc;
    }

    Acoustic_LocatingSignal_g_tc_particleSet_sequence._data._typeCode = (RTICdrTypeCode *)Acoustic_Particle_get_typecode();

    Acoustic_LocatingSignal_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&Acoustic_LocatingSignal_g_tc_sender_string;

    Acoustic_LocatingSignal_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&Acoustic_LocatingSignal_g_tc_receiver_string;

    Acoustic_LocatingSignal_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_ulonglong;

    Acoustic_LocatingSignal_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_short;

    Acoustic_LocatingSignal_g_tc_members[4]._representation._typeCode = (RTICdrTypeCode *)& Acoustic_LocatingSignal_g_tc_particleSet_sequence;
    Acoustic_LocatingSignal_g_tc_members[5]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_double;

    is_initialized = RTI_TRUE;

    return &Acoustic_LocatingSignal_g_tc;
}

RTIBool Acoustic_LocatingSignal_initialize(
    Acoustic_LocatingSignal* sample) {
    return Acoustic_LocatingSignal_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool Acoustic_LocatingSignal_initialize_ex(
    Acoustic_LocatingSignal* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return Acoustic_LocatingSignal_initialize_w_params(
        sample,&allocParams);

}

RTIBool Acoustic_LocatingSignal_initialize_w_params(
    Acoustic_LocatingSignal* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    void* buffer = NULL;
    if (buffer) {} /* To avoid warnings */

    if (allocParams) {} /* To avoid warnings */

    if (allocParams->allocate_memory){
        sample->sender= DDS_String_alloc ((64));
        if (sample->sender == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->sender!= NULL) { 
            sample->sender[0] = '\0';
        }
    }

    if (allocParams->allocate_memory){
        sample->receiver= DDS_String_alloc ((64));
        if (sample->receiver == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->receiver!= NULL) { 
            sample->receiver[0] = '\0';
        }
    }

    if (!RTICdrType_initUnsignedLongLong(&sample->timestamp)) {
        return RTI_FALSE;
    }     

    if (!RTICdrType_initShort(&sample->size)) {
        return RTI_FALSE;
    }     

    if (allocParams->allocate_memory) {
        Acoustic_ParticleSeq_initialize(&sample->particleSet );
        Acoustic_ParticleSeq_set_element_allocation_params(&sample->particleSet ,allocParams);
        if (!Acoustic_ParticleSeq_set_maximum(&sample->particleSet, (100))) {
            return RTI_FALSE;
        }
    } else { 
        Acoustic_ParticleSeq_set_length(&sample->particleSet, 0);
    }

    if (!RTICdrType_initDouble(&sample->error)) {
        return RTI_FALSE;
    }     

    return RTI_TRUE;
}

void Acoustic_LocatingSignal_finalize(
    Acoustic_LocatingSignal* sample)
{

    Acoustic_LocatingSignal_finalize_ex(sample,RTI_TRUE);
}

void Acoustic_LocatingSignal_finalize_ex(
    Acoustic_LocatingSignal* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    Acoustic_LocatingSignal_finalize_w_params(
        sample,&deallocParams);
}

void Acoustic_LocatingSignal_finalize_w_params(
    Acoustic_LocatingSignal* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }
    if (deallocParams) {} /* To avoid warnings */

    if (sample->sender != NULL) {
        DDS_String_free(sample->sender);
        sample->sender=NULL;

    }
    if (sample->receiver != NULL) {
        DDS_String_free(sample->receiver);
        sample->receiver=NULL;

    }

    Acoustic_ParticleSeq_set_element_deallocation_params(
        &sample->particleSet,deallocParams);
    Acoustic_ParticleSeq_finalize(&sample->particleSet);

}

void Acoustic_LocatingSignal_finalize_optional_members(
    Acoustic_LocatingSignal* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

    {
        DDS_UnsignedLong i, length;
        length = Acoustic_ParticleSeq_get_length(
            &sample->particleSet);

        for (i = 0; i < length; i++) {
            Acoustic_Particle_finalize_optional_members(
                Acoustic_ParticleSeq_get_reference(
                    &sample->particleSet, i), deallocParams->delete_pointers);
        }
    }  

}

RTIBool Acoustic_LocatingSignal_copy(
    Acoustic_LocatingSignal* dst,
    const Acoustic_LocatingSignal* src)
{

    if (!RTICdrType_copyStringEx (
        &dst->sender, src->sender, 
        (64) + 1, RTI_FALSE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->receiver, src->receiver, 
        (64) + 1, RTI_FALSE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyUnsignedLongLong (
        &dst->timestamp, &src->timestamp)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyShort (
        &dst->size, &src->size)) { 
        return RTI_FALSE;
    }
    if (!Acoustic_ParticleSeq_copy(&dst->particleSet ,
    &src->particleSet )) {
        return RTI_FALSE;
    }
    if (!RTICdrType_copyDouble (
        &dst->error, &src->error)) { 
        return RTI_FALSE;
    }

    return RTI_TRUE;
}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'Acoustic_LocatingSignal' sequence class.
*/
#define T Acoustic_LocatingSignal
#define TSeq Acoustic_LocatingSignalSeq
#define T_initialize_w_params Acoustic_LocatingSignal_initialize_w_params
#define T_finalize_w_params   Acoustic_LocatingSignal_finalize_w_params
#define T_copy       Acoustic_LocatingSignal_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params
#undef T_initialize_w_params
#undef TSeq
#undef T

