

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from LocatingSignal.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef LocatingSignal_890154112_h
#define LocatingSignal_890154112_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern "C" {

    extern const char *Acoustic_ParticleTYPENAME;

}

struct Acoustic_ParticleSeq;
#ifndef NDDS_STANDALONE_TYPE
class Acoustic_ParticleTypeSupport;
class Acoustic_ParticleDataWriter;
class Acoustic_ParticleDataReader;
#endif

class Acoustic_Particle 
{
  public:
    typedef struct Acoustic_ParticleSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef Acoustic_ParticleTypeSupport TypeSupport;
    typedef Acoustic_ParticleDataWriter DataWriter;
    typedef Acoustic_ParticleDataReader DataReader;
    #endif

    DDS_Double   x ;
    DDS_Double   y ;
    DDS_Double   z ;
    DDS_Double   w ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Acoustic_Particle_get_typecode(void); /* Type code */

DDS_SEQUENCE(Acoustic_ParticleSeq, Acoustic_Particle);                                        

NDDSUSERDllExport
RTIBool Acoustic_Particle_initialize(
    Acoustic_Particle* self);

NDDSUSERDllExport
RTIBool Acoustic_Particle_initialize_ex(
    Acoustic_Particle* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Acoustic_Particle_initialize_w_params(
    Acoustic_Particle* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void Acoustic_Particle_finalize(
    Acoustic_Particle* self);

NDDSUSERDllExport
void Acoustic_Particle_finalize_ex(
    Acoustic_Particle* self,RTIBool deletePointers);

NDDSUSERDllExport
void Acoustic_Particle_finalize_w_params(
    Acoustic_Particle* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Acoustic_Particle_finalize_optional_members(
    Acoustic_Particle* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Acoustic_Particle_copy(
    Acoustic_Particle* dst,
    const Acoustic_Particle* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif
extern "C" {

    extern const char *Acoustic_LocatingSignalTYPENAME;

}

struct Acoustic_LocatingSignalSeq;
#ifndef NDDS_STANDALONE_TYPE
class Acoustic_LocatingSignalTypeSupport;
class Acoustic_LocatingSignalDataWriter;
class Acoustic_LocatingSignalDataReader;
#endif

class Acoustic_LocatingSignal 
{
  public:
    typedef struct Acoustic_LocatingSignalSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef Acoustic_LocatingSignalTypeSupport TypeSupport;
    typedef Acoustic_LocatingSignalDataWriter DataWriter;
    typedef Acoustic_LocatingSignalDataReader DataReader;
    #endif

    DDS_Char *   sender ;
    DDS_Char *   receiver ;
    DDS_UnsignedLongLong   timestamp ;
    DDS_Short   size ;
    Acoustic_ParticleSeq  particleSet ;
    DDS_Double   error ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Acoustic_LocatingSignal_get_typecode(void); /* Type code */

DDS_SEQUENCE(Acoustic_LocatingSignalSeq, Acoustic_LocatingSignal);                                        

NDDSUSERDllExport
RTIBool Acoustic_LocatingSignal_initialize(
    Acoustic_LocatingSignal* self);

NDDSUSERDllExport
RTIBool Acoustic_LocatingSignal_initialize_ex(
    Acoustic_LocatingSignal* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Acoustic_LocatingSignal_initialize_w_params(
    Acoustic_LocatingSignal* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void Acoustic_LocatingSignal_finalize(
    Acoustic_LocatingSignal* self);

NDDSUSERDllExport
void Acoustic_LocatingSignal_finalize_ex(
    Acoustic_LocatingSignal* self,RTIBool deletePointers);

NDDSUSERDllExport
void Acoustic_LocatingSignal_finalize_w_params(
    Acoustic_LocatingSignal* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Acoustic_LocatingSignal_finalize_optional_members(
    Acoustic_LocatingSignal* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Acoustic_LocatingSignal_copy(
    Acoustic_LocatingSignal* dst,
    const Acoustic_LocatingSignal* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* LocatingSignal */

