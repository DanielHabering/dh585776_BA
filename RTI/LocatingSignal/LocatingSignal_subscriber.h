#include <stdio.h>
#include <stdlib.h>
#include <ndds/ndds_cpp.h>
#include "LocatingSignal.h"
#include "LocatingSignalSupport.h"

typedef void (*LocatingSignalCallback)(Acoustic_LocatingSignal *instance);

extern "C" int LocatingSignalSubscriber_init(int domainId, char* topicName, LocatingSignalCallback locSigPointer);
