#include <stdlib.h>
#include <stdio.h>
#include "LocatingSignal.h"
#include "LocatingSignalSupport.h"
#include <ndds/ndds_cpp.h>

void LocatingSignalWrite(Acoustic_LocatingSignal* instance);

int LocatingSignalPublisher_init(int domainId, char* topicName);