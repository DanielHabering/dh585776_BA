
/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from LocatingSignal.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include "LocatingSignalSupport.h"
#include "LocatingSignalPlugin.h"

#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif        

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'Acoustic_Particle' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   Acoustic_ParticleTYPENAME

/* Defines */
#define TDataWriter Acoustic_ParticleDataWriter
#define TData       Acoustic_Particle

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   Acoustic_ParticleTYPENAME

/* Defines */
#define TDataReader Acoustic_ParticleDataReader
#define TDataSeq    Acoustic_ParticleSeq
#define TData       Acoustic_Particle

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    Acoustic_ParticleTYPENAME
#define TPlugin_new  Acoustic_ParticlePlugin_new
#define TPlugin_delete  Acoustic_ParticlePlugin_delete

/* Defines */
#define TTypeSupport Acoustic_ParticleTypeSupport
#define TData        Acoustic_Particle
#define TDataReader  Acoustic_ParticleDataReader
#define TDataWriter  Acoustic_ParticleDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'Acoustic_LocatingSignal' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   Acoustic_LocatingSignalTYPENAME

/* Defines */
#define TDataWriter Acoustic_LocatingSignalDataWriter
#define TData       Acoustic_LocatingSignal

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   Acoustic_LocatingSignalTYPENAME

/* Defines */
#define TDataReader Acoustic_LocatingSignalDataReader
#define TDataSeq    Acoustic_LocatingSignalSeq
#define TData       Acoustic_LocatingSignal

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    Acoustic_LocatingSignalTYPENAME
#define TPlugin_new  Acoustic_LocatingSignalPlugin_new
#define TPlugin_delete  Acoustic_LocatingSignalPlugin_delete

/* Defines */
#define TTypeSupport Acoustic_LocatingSignalTypeSupport
#define TData        Acoustic_LocatingSignal
#define TDataReader  Acoustic_LocatingSignalDataReader
#define TDataWriter  Acoustic_LocatingSignalDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

