

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Connection.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif        

#ifndef cdr_type_h
#include "cdr/cdr_type.h"
#endif    

#ifndef osapi_heap_h
#include "osapi/osapi_heap.h" 
#endif
#else
#include "ndds_standalone_type.h"
#endif

#include "Connection.h"

/* ========================================================================= */
const char *Mapping_PosTYPENAME = "Mapping::Pos";

DDS_TypeCode* Mapping_Pos_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode_Member Mapping_Pos_g_tc_members[3]=
    {

        {
            (char *)"x",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"y",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"z",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode Mapping_Pos_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"Mapping::Pos", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            3, /* Number of members */
            Mapping_Pos_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for Mapping_Pos*/

    if (is_initialized) {
        return &Mapping_Pos_g_tc;
    }

    Mapping_Pos_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_double;

    Mapping_Pos_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_double;

    Mapping_Pos_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_double;

    is_initialized = RTI_TRUE;

    return &Mapping_Pos_g_tc;
}

RTIBool Mapping_Pos_initialize(
    Mapping_Pos* sample) {
    return Mapping_Pos_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool Mapping_Pos_initialize_ex(
    Mapping_Pos* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return Mapping_Pos_initialize_w_params(
        sample,&allocParams);

}

RTIBool Mapping_Pos_initialize_w_params(
    Mapping_Pos* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (allocParams) {} /* To avoid warnings */

    if (!RTICdrType_initDouble(&sample->x)) {
        return RTI_FALSE;
    }     

    if (!RTICdrType_initDouble(&sample->y)) {
        return RTI_FALSE;
    }     

    if (!RTICdrType_initDouble(&sample->z)) {
        return RTI_FALSE;
    }     

    return RTI_TRUE;
}

void Mapping_Pos_finalize(
    Mapping_Pos* sample)
{

    Mapping_Pos_finalize_ex(sample,RTI_TRUE);
}

void Mapping_Pos_finalize_ex(
    Mapping_Pos* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    Mapping_Pos_finalize_w_params(
        sample,&deallocParams);
}

void Mapping_Pos_finalize_w_params(
    Mapping_Pos* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }
    if (deallocParams) {} /* To avoid warnings */

}

void Mapping_Pos_finalize_optional_members(
    Mapping_Pos* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool Mapping_Pos_copy(
    Mapping_Pos* dst,
    const Mapping_Pos* src)
{

    if (!RTICdrType_copyDouble (
        &dst->x, &src->x)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyDouble (
        &dst->y, &src->y)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyDouble (
        &dst->z, &src->z)) { 
        return RTI_FALSE;
    }

    return RTI_TRUE;
}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'Mapping_Pos' sequence class.
*/
#define T Mapping_Pos
#define TSeq Mapping_PosSeq
#define T_initialize_w_params Mapping_Pos_initialize_w_params
#define T_finalize_w_params   Mapping_Pos_finalize_w_params
#define T_copy       Mapping_Pos_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params
#undef T_initialize_w_params
#undef TSeq
#undef T

/* ========================================================================= */
const char *Mapping_QualityTYPENAME = "Mapping::Quality";

DDS_TypeCode* Mapping_Quality_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode_Member Mapping_Quality_g_tc_members[3]=
    {

        {
            (char *)"CONNECTION",/* Member name */
            {
                0, /* Ignored */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            CONNECTION, /* Enumerator ordinal */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PRIVATE_MEMBER,/* Member visibility */ 

            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"NO_CONNECTION",/* Member name */
            {
                0, /* Ignored */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            NO_CONNECTION, /* Enumerator ordinal */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PRIVATE_MEMBER,/* Member visibility */ 

            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"WEAK_CONNECTION",/* Member name */
            {
                0, /* Ignored */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            WEAK_CONNECTION, /* Enumerator ordinal */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PRIVATE_MEMBER,/* Member visibility */ 

            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode Mapping_Quality_g_tc =
    {{
            DDS_TK_ENUM,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"Mapping::Quality", /* Name */
            NULL,     /* Base class type code is assigned later */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            3, /* Number of members */
            Mapping_Quality_g_tc_members, /* Members */
            DDS_VM_NONE   /* Type Modifier */        
        }}; /* Type code for Mapping_Quality*/

    if (is_initialized) {
        return &Mapping_Quality_g_tc;
    }

    is_initialized = RTI_TRUE;

    return &Mapping_Quality_g_tc;
}

RTIBool Mapping_Quality_initialize(
    Mapping_Quality* sample) {
    *sample = CONNECTION;
    return RTI_TRUE;
}

RTIBool Mapping_Quality_initialize_ex(
    Mapping_Quality* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return Mapping_Quality_initialize_w_params(
        sample,&allocParams);

}

RTIBool Mapping_Quality_initialize_w_params(
    Mapping_Quality* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (allocParams) {} /* To avoid warnings */
    *sample = CONNECTION;
    return RTI_TRUE;
}

void Mapping_Quality_finalize(
    Mapping_Quality* sample)
{

    if (sample==NULL) {
        return;
    }
}

void Mapping_Quality_finalize_ex(
    Mapping_Quality* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    Mapping_Quality_finalize_w_params(
        sample,&deallocParams);
}

void Mapping_Quality_finalize_w_params(
    Mapping_Quality* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }
    if (deallocParams) {} /* To avoid warnings */

}

void Mapping_Quality_finalize_optional_members(
    Mapping_Quality* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool Mapping_Quality_copy(
    Mapping_Quality* dst,
    const Mapping_Quality* src)
{

    return RTICdrType_copyEnum((RTICdrEnum *)dst, (RTICdrEnum *)src);

}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'Mapping_Quality' sequence class.
*/
#define T Mapping_Quality
#define TSeq Mapping_QualitySeq
#define T_initialize_w_params Mapping_Quality_initialize_w_params
#define T_finalize_w_params   Mapping_Quality_finalize_w_params
#define T_copy       Mapping_Quality_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params
#undef T_initialize_w_params
#undef TSeq
#undef T

/* ========================================================================= */
const char *Mapping_ConnectionTYPENAME = "Mapping::Connection";

DDS_TypeCode* Mapping_Connection_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode_Member Mapping_Connection_g_tc_members[3]=
    {

        {
            (char *)"apuA",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"apuB",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"qual",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode Mapping_Connection_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"Mapping::Connection", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            3, /* Number of members */
            Mapping_Connection_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for Mapping_Connection*/

    if (is_initialized) {
        return &Mapping_Connection_g_tc;
    }

    Mapping_Connection_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)Mapping_Pos_get_typecode();

    Mapping_Connection_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)Mapping_Pos_get_typecode();

    Mapping_Connection_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)Mapping_Quality_get_typecode();

    is_initialized = RTI_TRUE;

    return &Mapping_Connection_g_tc;
}

RTIBool Mapping_Connection_initialize(
    Mapping_Connection* sample) {
    return Mapping_Connection_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool Mapping_Connection_initialize_ex(
    Mapping_Connection* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return Mapping_Connection_initialize_w_params(
        sample,&allocParams);

}

RTIBool Mapping_Connection_initialize_w_params(
    Mapping_Connection* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (allocParams) {} /* To avoid warnings */

    if (!Mapping_Pos_initialize_w_params(&sample->apuA,
    allocParams)) {
        return RTI_FALSE;
    }
    if (!Mapping_Pos_initialize_w_params(&sample->apuB,
    allocParams)) {
        return RTI_FALSE;
    }
    if (!Mapping_Quality_initialize_w_params(&sample->qual,
    allocParams)) {
        return RTI_FALSE;
    }
    return RTI_TRUE;
}

void Mapping_Connection_finalize(
    Mapping_Connection* sample)
{

    Mapping_Connection_finalize_ex(sample,RTI_TRUE);
}

void Mapping_Connection_finalize_ex(
    Mapping_Connection* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    Mapping_Connection_finalize_w_params(
        sample,&deallocParams);
}

void Mapping_Connection_finalize_w_params(
    Mapping_Connection* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }
    if (deallocParams) {} /* To avoid warnings */

    Mapping_Pos_finalize_w_params(&sample->apuA,deallocParams);

    Mapping_Pos_finalize_w_params(&sample->apuB,deallocParams);

    Mapping_Quality_finalize_w_params(&sample->qual,deallocParams);

}

void Mapping_Connection_finalize_optional_members(
    Mapping_Connection* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

    Mapping_Pos_finalize_optional_members(&sample->apuA, deallocParams->delete_pointers);
    Mapping_Pos_finalize_optional_members(&sample->apuB, deallocParams->delete_pointers);
    Mapping_Quality_finalize_optional_members(&sample->qual, deallocParams->delete_pointers);
}

RTIBool Mapping_Connection_copy(
    Mapping_Connection* dst,
    const Mapping_Connection* src)
{

    if (!Mapping_Pos_copy(
        &dst->apuA, &src->apuA)) {
        return RTI_FALSE;
    } 
    if (!Mapping_Pos_copy(
        &dst->apuB, &src->apuB)) {
        return RTI_FALSE;
    } 
    if (!Mapping_Quality_copy(
        &dst->qual, &src->qual)) {
        return RTI_FALSE;
    } 

    return RTI_TRUE;
}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'Mapping_Connection' sequence class.
*/
#define T Mapping_Connection
#define TSeq Mapping_ConnectionSeq
#define T_initialize_w_params Mapping_Connection_initialize_w_params
#define T_finalize_w_params   Mapping_Connection_finalize_w_params
#define T_copy       Mapping_Connection_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params
#undef T_initialize_w_params
#undef TSeq
#undef T

