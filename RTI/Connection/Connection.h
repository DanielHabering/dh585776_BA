

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Connection.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Connection_267070610_h
#define Connection_267070610_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern "C" {

    extern const char *Mapping_PosTYPENAME;

}

struct Mapping_PosSeq;
#ifndef NDDS_STANDALONE_TYPE
class Mapping_PosTypeSupport;
class Mapping_PosDataWriter;
class Mapping_PosDataReader;
#endif

class Mapping_Pos 
{
  public:
    typedef struct Mapping_PosSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef Mapping_PosTypeSupport TypeSupport;
    typedef Mapping_PosDataWriter DataWriter;
    typedef Mapping_PosDataReader DataReader;
    #endif

    DDS_Double   x ;
    DDS_Double   y ;
    DDS_Double   z ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Mapping_Pos_get_typecode(void); /* Type code */

DDS_SEQUENCE(Mapping_PosSeq, Mapping_Pos);                                        

NDDSUSERDllExport
RTIBool Mapping_Pos_initialize(
    Mapping_Pos* self);

NDDSUSERDllExport
RTIBool Mapping_Pos_initialize_ex(
    Mapping_Pos* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Mapping_Pos_initialize_w_params(
    Mapping_Pos* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void Mapping_Pos_finalize(
    Mapping_Pos* self);

NDDSUSERDllExport
void Mapping_Pos_finalize_ex(
    Mapping_Pos* self,RTIBool deletePointers);

NDDSUSERDllExport
void Mapping_Pos_finalize_w_params(
    Mapping_Pos* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Mapping_Pos_finalize_optional_members(
    Mapping_Pos* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Mapping_Pos_copy(
    Mapping_Pos* dst,
    const Mapping_Pos* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif
typedef enum Mapping_Quality
{
    CONNECTION ,      
    NO_CONNECTION ,      
    WEAK_CONNECTION      
} Mapping_Quality;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Mapping_Quality_get_typecode(void); /* Type code */

DDS_SEQUENCE(Mapping_QualitySeq, Mapping_Quality);                                        

NDDSUSERDllExport
RTIBool Mapping_Quality_initialize(
    Mapping_Quality* self);

NDDSUSERDllExport
RTIBool Mapping_Quality_initialize_ex(
    Mapping_Quality* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Mapping_Quality_initialize_w_params(
    Mapping_Quality* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void Mapping_Quality_finalize(
    Mapping_Quality* self);

NDDSUSERDllExport
void Mapping_Quality_finalize_ex(
    Mapping_Quality* self,RTIBool deletePointers);

NDDSUSERDllExport
void Mapping_Quality_finalize_w_params(
    Mapping_Quality* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Mapping_Quality_finalize_optional_members(
    Mapping_Quality* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Mapping_Quality_copy(
    Mapping_Quality* dst,
    const Mapping_Quality* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif
extern "C" {

    extern const char *Mapping_ConnectionTYPENAME;

}

struct Mapping_ConnectionSeq;
#ifndef NDDS_STANDALONE_TYPE
class Mapping_ConnectionTypeSupport;
class Mapping_ConnectionDataWriter;
class Mapping_ConnectionDataReader;
#endif

class Mapping_Connection 
{
  public:
    typedef struct Mapping_ConnectionSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef Mapping_ConnectionTypeSupport TypeSupport;
    typedef Mapping_ConnectionDataWriter DataWriter;
    typedef Mapping_ConnectionDataReader DataReader;
    #endif

    Mapping_Pos   apuA ;
    Mapping_Pos   apuB ;
    Mapping_Quality   qual ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Mapping_Connection_get_typecode(void); /* Type code */

DDS_SEQUENCE(Mapping_ConnectionSeq, Mapping_Connection);                                        

NDDSUSERDllExport
RTIBool Mapping_Connection_initialize(
    Mapping_Connection* self);

NDDSUSERDllExport
RTIBool Mapping_Connection_initialize_ex(
    Mapping_Connection* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Mapping_Connection_initialize_w_params(
    Mapping_Connection* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void Mapping_Connection_finalize(
    Mapping_Connection* self);

NDDSUSERDllExport
void Mapping_Connection_finalize_ex(
    Mapping_Connection* self,RTIBool deletePointers);

NDDSUSERDllExport
void Mapping_Connection_finalize_w_params(
    Mapping_Connection* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Mapping_Connection_finalize_optional_members(
    Mapping_Connection* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Mapping_Connection_copy(
    Mapping_Connection* dst,
    const Mapping_Connection* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* Connection */

