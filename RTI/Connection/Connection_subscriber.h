#include <stdio.h>
#include <stdlib.h>
#include "ndds/ndds_cpp.h"
#include "../../RTI/Connection/Connection.h"
#include "../../RTI/Connection/ConnectionSupport.h"
typedef void (*callback_function)(Mapping_Connection *instance);
extern "C" int ConnectionSubscriber_init(int domainId, char* topicName, callback_function callbackPointer);
