

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Connection.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef ConnectionPlugin_267070610_h
#define ConnectionPlugin_267070610_h

#include "Connection.h"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

extern "C" {

    #define Mapping_PosPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define Mapping_PosPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define Mapping_PosPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define Mapping_PosPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define Mapping_PosPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern Mapping_Pos*
    Mapping_PosPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern Mapping_Pos*
    Mapping_PosPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern Mapping_Pos*
    Mapping_PosPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    Mapping_PosPluginSupport_copy_data(
        Mapping_Pos *out,
        const Mapping_Pos *in);

    NDDSUSERDllExport extern void 
    Mapping_PosPluginSupport_destroy_data_w_params(
        Mapping_Pos *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    Mapping_PosPluginSupport_destroy_data_ex(
        Mapping_Pos *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    Mapping_PosPluginSupport_destroy_data(
        Mapping_Pos *sample);

    NDDSUSERDllExport extern void 
    Mapping_PosPluginSupport_print_data(
        const Mapping_Pos *sample,
        const char *desc,
        unsigned int indent);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    Mapping_PosPlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    Mapping_PosPlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    Mapping_PosPlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    Mapping_PosPlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    Mapping_PosPlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Pos *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    Mapping_PosPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Pos *out,
        const Mapping_Pos *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    Mapping_PosPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const Mapping_Pos *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Mapping_PosPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Pos *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Mapping_PosPlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const Mapping_Pos *sample); 

    NDDSUSERDllExport extern RTIBool 
    Mapping_PosPlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Pos **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Mapping_PosPlugin_deserialize_from_cdr_buffer(
        Mapping_Pos *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    Mapping_PosPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    Mapping_PosPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    Mapping_PosPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Mapping_PosPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    Mapping_PosPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const Mapping_Pos * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    Mapping_PosPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    Mapping_PosPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Mapping_PosPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    Mapping_PosPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const Mapping_Pos *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Mapping_PosPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Pos * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Mapping_PosPlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Pos ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Mapping_PosPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Pos *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    Mapping_PosPlugin_new(void);

    NDDSUSERDllExport extern void
    Mapping_PosPlugin_delete(struct PRESTypePlugin *);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    Mapping_QualityPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const Mapping_Quality *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Mapping_QualityPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Quality *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Mapping_QualityPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    Mapping_QualityPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    Mapping_QualityPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Mapping_QualityPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    Mapping_QualityPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const Mapping_Quality * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern unsigned int 
    Mapping_QualityPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Mapping_QualityPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    Mapping_QualityPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const Mapping_Quality *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Mapping_QualityPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Quality * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Mapping_QualityPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Quality *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    /* ----------------------------------------------------------------------------
    Support functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern void
    Mapping_QualityPluginSupport_print_data(
        const Mapping_Quality *sample, const char *desc, int indent_level);

    #define Mapping_ConnectionPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define Mapping_ConnectionPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define Mapping_ConnectionPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define Mapping_ConnectionPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define Mapping_ConnectionPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern Mapping_Connection*
    Mapping_ConnectionPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern Mapping_Connection*
    Mapping_ConnectionPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern Mapping_Connection*
    Mapping_ConnectionPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    Mapping_ConnectionPluginSupport_copy_data(
        Mapping_Connection *out,
        const Mapping_Connection *in);

    NDDSUSERDllExport extern void 
    Mapping_ConnectionPluginSupport_destroy_data_w_params(
        Mapping_Connection *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    Mapping_ConnectionPluginSupport_destroy_data_ex(
        Mapping_Connection *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    Mapping_ConnectionPluginSupport_destroy_data(
        Mapping_Connection *sample);

    NDDSUSERDllExport extern void 
    Mapping_ConnectionPluginSupport_print_data(
        const Mapping_Connection *sample,
        const char *desc,
        unsigned int indent);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    Mapping_ConnectionPlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    Mapping_ConnectionPlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    Mapping_ConnectionPlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    Mapping_ConnectionPlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    Mapping_ConnectionPlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Connection *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    Mapping_ConnectionPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Connection *out,
        const Mapping_Connection *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    Mapping_ConnectionPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const Mapping_Connection *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Mapping_ConnectionPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Connection *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Mapping_ConnectionPlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const Mapping_Connection *sample); 

    NDDSUSERDllExport extern RTIBool 
    Mapping_ConnectionPlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Connection **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Mapping_ConnectionPlugin_deserialize_from_cdr_buffer(
        Mapping_Connection *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    Mapping_ConnectionPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    Mapping_ConnectionPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    Mapping_ConnectionPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Mapping_ConnectionPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    Mapping_ConnectionPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const Mapping_Connection * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    Mapping_ConnectionPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    Mapping_ConnectionPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Mapping_ConnectionPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    Mapping_ConnectionPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const Mapping_Connection *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Mapping_ConnectionPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Connection * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Mapping_ConnectionPlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Connection ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Mapping_ConnectionPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        Mapping_Connection *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    Mapping_ConnectionPlugin_new(void);

    NDDSUSERDllExport extern void
    Mapping_ConnectionPlugin_delete(struct PRESTypePlugin *);

}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* ConnectionPlugin_267070610_h */

