#include <stdio.h>
#include <stdlib.h>
#include "Connection.h"
#include "ConnectionSupport.h"
#include "ndds/ndds_cpp.h"

int ConnectionPublisher_init(int domainId, char* topicName);
int Connection_write(Mapping_Connection *instance);

