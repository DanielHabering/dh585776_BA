#include <stdlib.h>
#include <stdio.h>
#include <ndds/ndds_cpp.h>
#include "RuntimeRequest.h"
#include "RuntimeRequestSupport.h"

int RuntimeRequest_write(gazebo_RuntimeRequest* instance);
int RuntimeRequestPublisher_init(int domainId, char* topicName);