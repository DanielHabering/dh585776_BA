#include <stdio.h>
#include <stdlib.h>
#include "PositionRequest.h"
#include "PositionRequestSupport.h"
#include <ndds/ndds_cpp.h>

void PositionRequestWrite(char* apu, int content);
int PositionRequestPublisher_init(int domainId, char* topicName);