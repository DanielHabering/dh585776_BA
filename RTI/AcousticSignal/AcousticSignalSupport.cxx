
/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from AcousticSignal.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include "AcousticSignalSupport.h"
#include "AcousticSignalPlugin.h"

#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif        

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'Acoustic_Particle' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   Acoustic_ParticleTYPENAME

/* Defines */
#define TDataWriter Acoustic_ParticleDataWriter
#define TData       Acoustic_Particle

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   Acoustic_ParticleTYPENAME

/* Defines */
#define TDataReader Acoustic_ParticleDataReader
#define TDataSeq    Acoustic_ParticleSeq
#define TData       Acoustic_Particle

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    Acoustic_ParticleTYPENAME
#define TPlugin_new  Acoustic_ParticlePlugin_new
#define TPlugin_delete  Acoustic_ParticlePlugin_delete

/* Defines */
#define TTypeSupport Acoustic_ParticleTypeSupport
#define TData        Acoustic_Particle
#define TDataReader  Acoustic_ParticleDataReader
#define TDataWriter  Acoustic_ParticleDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'Acoustic_AcousticSignal' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   Acoustic_AcousticSignalTYPENAME

/* Defines */
#define TDataWriter Acoustic_AcousticSignalDataWriter
#define TData       Acoustic_AcousticSignal

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   Acoustic_AcousticSignalTYPENAME

/* Defines */
#define TDataReader Acoustic_AcousticSignalDataReader
#define TDataSeq    Acoustic_AcousticSignalSeq
#define TData       Acoustic_AcousticSignal

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    Acoustic_AcousticSignalTYPENAME
#define TPlugin_new  Acoustic_AcousticSignalPlugin_new
#define TPlugin_delete  Acoustic_AcousticSignalPlugin_delete

/* Defines */
#define TTypeSupport Acoustic_AcousticSignalTypeSupport
#define TData        Acoustic_AcousticSignal
#define TDataReader  Acoustic_AcousticSignalDataReader
#define TDataWriter  Acoustic_AcousticSignalDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

