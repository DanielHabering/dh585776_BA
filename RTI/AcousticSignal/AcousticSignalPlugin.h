

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from AcousticSignal.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef AcousticSignalPlugin_1988540598_h
#define AcousticSignalPlugin_1988540598_h

#include "AcousticSignal.h"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

extern "C" {

    #define Acoustic_ParticlePlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define Acoustic_ParticlePlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define Acoustic_ParticlePlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define Acoustic_ParticlePlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define Acoustic_ParticlePlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern Acoustic_Particle*
    Acoustic_ParticlePluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern Acoustic_Particle*
    Acoustic_ParticlePluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern Acoustic_Particle*
    Acoustic_ParticlePluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_ParticlePluginSupport_copy_data(
        Acoustic_Particle *out,
        const Acoustic_Particle *in);

    NDDSUSERDllExport extern void 
    Acoustic_ParticlePluginSupport_destroy_data_w_params(
        Acoustic_Particle *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    Acoustic_ParticlePluginSupport_destroy_data_ex(
        Acoustic_Particle *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    Acoustic_ParticlePluginSupport_destroy_data(
        Acoustic_Particle *sample);

    NDDSUSERDllExport extern void 
    Acoustic_ParticlePluginSupport_print_data(
        const Acoustic_Particle *sample,
        const char *desc,
        unsigned int indent);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    Acoustic_ParticlePlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    Acoustic_ParticlePlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    Acoustic_ParticlePlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    Acoustic_ParticlePlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    Acoustic_ParticlePlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_Particle *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    Acoustic_ParticlePlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_Particle *out,
        const Acoustic_Particle *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    Acoustic_ParticlePlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const Acoustic_Particle *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_ParticlePlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_Particle *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Acoustic_ParticlePlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const Acoustic_Particle *sample); 

    NDDSUSERDllExport extern RTIBool 
    Acoustic_ParticlePlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_Particle **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Acoustic_ParticlePlugin_deserialize_from_cdr_buffer(
        Acoustic_Particle *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    Acoustic_ParticlePlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    Acoustic_ParticlePlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    Acoustic_ParticlePlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Acoustic_ParticlePlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    Acoustic_ParticlePlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const Acoustic_Particle * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    Acoustic_ParticlePlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    Acoustic_ParticlePlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Acoustic_ParticlePlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_ParticlePlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const Acoustic_Particle *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_ParticlePlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_Particle * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_ParticlePlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_Particle ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Acoustic_ParticlePlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_Particle *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    Acoustic_ParticlePlugin_new(void);

    NDDSUSERDllExport extern void
    Acoustic_ParticlePlugin_delete(struct PRESTypePlugin *);

    #define Acoustic_AcousticSignalPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
    #define Acoustic_AcousticSignalPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
    #define Acoustic_AcousticSignalPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

    #define Acoustic_AcousticSignalPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
    #define Acoustic_AcousticSignalPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

    /* --------------------------------------------------------------------------------------
    Support functions:
    * -------------------------------------------------------------------------------------- */

    NDDSUSERDllExport extern Acoustic_AcousticSignal*
    Acoustic_AcousticSignalPluginSupport_create_data_w_params(
        const struct DDS_TypeAllocationParams_t * alloc_params);

    NDDSUSERDllExport extern Acoustic_AcousticSignal*
    Acoustic_AcousticSignalPluginSupport_create_data_ex(RTIBool allocate_pointers);

    NDDSUSERDllExport extern Acoustic_AcousticSignal*
    Acoustic_AcousticSignalPluginSupport_create_data(void);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_AcousticSignalPluginSupport_copy_data(
        Acoustic_AcousticSignal *out,
        const Acoustic_AcousticSignal *in);

    NDDSUSERDllExport extern void 
    Acoustic_AcousticSignalPluginSupport_destroy_data_w_params(
        Acoustic_AcousticSignal *sample,
        const struct DDS_TypeDeallocationParams_t * dealloc_params);

    NDDSUSERDllExport extern void 
    Acoustic_AcousticSignalPluginSupport_destroy_data_ex(
        Acoustic_AcousticSignal *sample,RTIBool deallocate_pointers);

    NDDSUSERDllExport extern void 
    Acoustic_AcousticSignalPluginSupport_destroy_data(
        Acoustic_AcousticSignal *sample);

    NDDSUSERDllExport extern void 
    Acoustic_AcousticSignalPluginSupport_print_data(
        const Acoustic_AcousticSignal *sample,
        const char *desc,
        unsigned int indent);

    /* ----------------------------------------------------------------------------
    Callback functions:
    * ---------------------------------------------------------------------------- */

    NDDSUSERDllExport extern PRESTypePluginParticipantData 
    Acoustic_AcousticSignalPlugin_on_participant_attached(
        void *registration_data, 
        const struct PRESTypePluginParticipantInfo *participant_info,
        RTIBool top_level_registration, 
        void *container_plugin_context,
        RTICdrTypeCode *typeCode);

    NDDSUSERDllExport extern void 
    Acoustic_AcousticSignalPlugin_on_participant_detached(
        PRESTypePluginParticipantData participant_data);

    NDDSUSERDllExport extern PRESTypePluginEndpointData 
    Acoustic_AcousticSignalPlugin_on_endpoint_attached(
        PRESTypePluginParticipantData participant_data,
        const struct PRESTypePluginEndpointInfo *endpoint_info,
        RTIBool top_level_registration, 
        void *container_plugin_context);

    NDDSUSERDllExport extern void 
    Acoustic_AcousticSignalPlugin_on_endpoint_detached(
        PRESTypePluginEndpointData endpoint_data);

    NDDSUSERDllExport extern void    
    Acoustic_AcousticSignalPlugin_return_sample(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_AcousticSignal *sample,
        void *handle);    

    NDDSUSERDllExport extern RTIBool 
    Acoustic_AcousticSignalPlugin_copy_sample(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_AcousticSignal *out,
        const Acoustic_AcousticSignal *in);

    /* ----------------------------------------------------------------------------
    (De)Serialize functions:
    * ------------------------------------------------------------------------- */

    NDDSUSERDllExport extern RTIBool 
    Acoustic_AcousticSignalPlugin_serialize(
        PRESTypePluginEndpointData endpoint_data,
        const Acoustic_AcousticSignal *sample,
        struct RTICdrStream *stream, 
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_AcousticSignalPlugin_deserialize_sample(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_AcousticSignal *sample, 
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Acoustic_AcousticSignalPlugin_serialize_to_cdr_buffer(
        char * buffer,
        unsigned int * length,
        const Acoustic_AcousticSignal *sample); 

    NDDSUSERDllExport extern RTIBool 
    Acoustic_AcousticSignalPlugin_deserialize(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_AcousticSignal **sample, 
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Acoustic_AcousticSignalPlugin_deserialize_from_cdr_buffer(
        Acoustic_AcousticSignal *sample,
        const char * buffer,
        unsigned int length);    

    NDDSUSERDllExport extern RTIBool
    Acoustic_AcousticSignalPlugin_skip(
        PRESTypePluginEndpointData endpoint_data,
        struct RTICdrStream *stream, 
        RTIBool skip_encapsulation,  
        RTIBool skip_sample, 
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern unsigned int 
    Acoustic_AcousticSignalPlugin_get_serialized_sample_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);    

    NDDSUSERDllExport extern unsigned int 
    Acoustic_AcousticSignalPlugin_get_serialized_sample_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Acoustic_AcousticSignalPlugin_get_serialized_sample_min_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int
    Acoustic_AcousticSignalPlugin_get_serialized_sample_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment,
        const Acoustic_AcousticSignal * sample);

    /* --------------------------------------------------------------------------------------
    Key Management functions:
    * -------------------------------------------------------------------------------------- */
    NDDSUSERDllExport extern PRESTypePluginKeyKind 
    Acoustic_AcousticSignalPlugin_get_key_kind(void);

    NDDSUSERDllExport extern unsigned int 
    Acoustic_AcousticSignalPlugin_get_serialized_key_max_size_ex(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool * overflow,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern unsigned int 
    Acoustic_AcousticSignalPlugin_get_serialized_key_max_size(
        PRESTypePluginEndpointData endpoint_data,
        RTIBool include_encapsulation,
        RTIEncapsulationId encapsulation_id,
        unsigned int current_alignment);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_AcousticSignalPlugin_serialize_key(
        PRESTypePluginEndpointData endpoint_data,
        const Acoustic_AcousticSignal *sample,
        struct RTICdrStream *stream,
        RTIBool serialize_encapsulation,
        RTIEncapsulationId encapsulation_id,
        RTIBool serialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_AcousticSignalPlugin_deserialize_key_sample(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_AcousticSignal * sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool 
    Acoustic_AcousticSignalPlugin_deserialize_key(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_AcousticSignal ** sample,
        RTIBool * drop_sample,
        struct RTICdrStream *stream,
        RTIBool deserialize_encapsulation,
        RTIBool deserialize_key,
        void *endpoint_plugin_qos);

    NDDSUSERDllExport extern RTIBool
    Acoustic_AcousticSignalPlugin_serialized_sample_to_key(
        PRESTypePluginEndpointData endpoint_data,
        Acoustic_AcousticSignal *sample,
        struct RTICdrStream *stream, 
        RTIBool deserialize_encapsulation,  
        RTIBool deserialize_key, 
        void *endpoint_plugin_qos);

    /* Plugin Functions */
    NDDSUSERDllExport extern struct PRESTypePlugin*
    Acoustic_AcousticSignalPlugin_new(void);

    NDDSUSERDllExport extern void
    Acoustic_AcousticSignalPlugin_delete(struct PRESTypePlugin *);

}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* AcousticSignalPlugin_1988540598_h */

