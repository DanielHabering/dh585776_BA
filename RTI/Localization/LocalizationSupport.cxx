
/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Localization.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include "LocalizationSupport.h"
#include "LocalizationPlugin.h"

#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif        

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'de_dlr_enex_datamodel_range_Point' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   de_dlr_enex_datamodel_range_PointTYPENAME

/* Defines */
#define TDataWriter de_dlr_enex_datamodel_range_PointDataWriter
#define TData       de_dlr_enex_datamodel_range_Point

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   de_dlr_enex_datamodel_range_PointTYPENAME

/* Defines */
#define TDataReader de_dlr_enex_datamodel_range_PointDataReader
#define TDataSeq    de_dlr_enex_datamodel_range_PointSeq
#define TData       de_dlr_enex_datamodel_range_Point

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    de_dlr_enex_datamodel_range_PointTYPENAME
#define TPlugin_new  de_dlr_enex_datamodel_range_PointPlugin_new
#define TPlugin_delete  de_dlr_enex_datamodel_range_PointPlugin_delete

/* Defines */
#define TTypeSupport de_dlr_enex_datamodel_range_PointTypeSupport
#define TData        de_dlr_enex_datamodel_range_Point
#define TDataReader  de_dlr_enex_datamodel_range_PointDataReader
#define TDataWriter  de_dlr_enex_datamodel_range_PointDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'de_dlr_enex_datamodel_range_Quaternion' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   de_dlr_enex_datamodel_range_QuaternionTYPENAME

/* Defines */
#define TDataWriter de_dlr_enex_datamodel_range_QuaternionDataWriter
#define TData       de_dlr_enex_datamodel_range_Quaternion

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   de_dlr_enex_datamodel_range_QuaternionTYPENAME

/* Defines */
#define TDataReader de_dlr_enex_datamodel_range_QuaternionDataReader
#define TDataSeq    de_dlr_enex_datamodel_range_QuaternionSeq
#define TData       de_dlr_enex_datamodel_range_Quaternion

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    de_dlr_enex_datamodel_range_QuaternionTYPENAME
#define TPlugin_new  de_dlr_enex_datamodel_range_QuaternionPlugin_new
#define TPlugin_delete  de_dlr_enex_datamodel_range_QuaternionPlugin_delete

/* Defines */
#define TTypeSupport de_dlr_enex_datamodel_range_QuaternionTypeSupport
#define TData        de_dlr_enex_datamodel_range_Quaternion
#define TDataReader  de_dlr_enex_datamodel_range_QuaternionDataReader
#define TDataWriter  de_dlr_enex_datamodel_range_QuaternionDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'de_dlr_enex_datamodel_range_Pose' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   de_dlr_enex_datamodel_range_PoseTYPENAME

/* Defines */
#define TDataWriter de_dlr_enex_datamodel_range_PoseDataWriter
#define TData       de_dlr_enex_datamodel_range_Pose

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   de_dlr_enex_datamodel_range_PoseTYPENAME

/* Defines */
#define TDataReader de_dlr_enex_datamodel_range_PoseDataReader
#define TDataSeq    de_dlr_enex_datamodel_range_PoseSeq
#define TData       de_dlr_enex_datamodel_range_Pose

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    de_dlr_enex_datamodel_range_PoseTYPENAME
#define TPlugin_new  de_dlr_enex_datamodel_range_PosePlugin_new
#define TPlugin_delete  de_dlr_enex_datamodel_range_PosePlugin_delete

/* Defines */
#define TTypeSupport de_dlr_enex_datamodel_range_PoseTypeSupport
#define TData        de_dlr_enex_datamodel_range_Pose
#define TDataReader  de_dlr_enex_datamodel_range_PoseDataReader
#define TDataWriter  de_dlr_enex_datamodel_range_PoseDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

/* ========================================================================= */
/**
<<IMPLEMENTATION>>

Defines:   TData,
TDataWriter,
TDataReader,
TTypeSupport

Configure and implement 'de_dlr_enex_datamodel_range_Localization' support classes.

Note: Only the #defined classes get defined
*/

/* ----------------------------------------------------------------- */
/* DDSDataWriter
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataWriter, TData
*/

/* Requires */
#define TTYPENAME   de_dlr_enex_datamodel_range_LocalizationTYPENAME

/* Defines */
#define TDataWriter de_dlr_enex_datamodel_range_LocalizationDataWriter
#define TData       de_dlr_enex_datamodel_range_Localization

#include "dds_cpp/generic/dds_cpp_data_TDataWriter.gen"

#undef TDataWriter
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* DDSDataReader
*/

/**
<<IMPLEMENTATION >>

Defines:   TDataReader, TDataSeq, TData
*/

/* Requires */
#define TTYPENAME   de_dlr_enex_datamodel_range_LocalizationTYPENAME

/* Defines */
#define TDataReader de_dlr_enex_datamodel_range_LocalizationDataReader
#define TDataSeq    de_dlr_enex_datamodel_range_LocalizationSeq
#define TData       de_dlr_enex_datamodel_range_Localization

#include "dds_cpp/generic/dds_cpp_data_TDataReader.gen"

#undef TDataReader
#undef TDataSeq
#undef TData

#undef TTYPENAME

/* ----------------------------------------------------------------- */
/* TypeSupport

<<IMPLEMENTATION >>

Requires:  TTYPENAME,
TPlugin_new
TPlugin_delete
Defines:   TTypeSupport, TData, TDataReader, TDataWriter
*/

/* Requires */
#define TTYPENAME    de_dlr_enex_datamodel_range_LocalizationTYPENAME
#define TPlugin_new  de_dlr_enex_datamodel_range_LocalizationPlugin_new
#define TPlugin_delete  de_dlr_enex_datamodel_range_LocalizationPlugin_delete

/* Defines */
#define TTypeSupport de_dlr_enex_datamodel_range_LocalizationTypeSupport
#define TData        de_dlr_enex_datamodel_range_Localization
#define TDataReader  de_dlr_enex_datamodel_range_LocalizationDataReader
#define TDataWriter  de_dlr_enex_datamodel_range_LocalizationDataWriter
#define TGENERATE_SER_CODE
#define TGENERATE_TYPECODE

#include "dds_cpp/generic/dds_cpp_data_TTypeSupport.gen"

#undef TTypeSupport
#undef TData
#undef TDataReader
#undef TDataWriter
#undef TGENERATE_TYPECODE
#undef TGENERATE_SER_CODE
#undef TTYPENAME
#undef TPlugin_new
#undef TPlugin_delete

