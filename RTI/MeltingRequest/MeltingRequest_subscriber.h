#include <stdio.h>
#include <stdlib.h>
#include <ndds/ndds_cpp.h>
#include "MeltingRequest.h"
#include "MeltingRequestSupport.h"

typedef void (*callback_function)(Melting_MeltRequest *instance);

extern "C" int MeltRequest_init(int domainId, char* topicName, callback_function callbackPointer);

