#include <stdio.h>
#include <stdlib.h>
#include "ndds/ndds_cpp.h"
#include "Ping.h"
#include "PingSupport.h"

extern "C" int PingPublisher_init(int domainId, char* topicName);

int PingWrite(char* sender, char* receiver, long int timestamp);
