

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Ping.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Ping_364684304_h
#define Ping_364684304_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern "C" {

    extern const char *Acoustic_PingTYPENAME;

}

struct Acoustic_PingSeq;
#ifndef NDDS_STANDALONE_TYPE
class Acoustic_PingTypeSupport;
class Acoustic_PingDataWriter;
class Acoustic_PingDataReader;
#endif

class Acoustic_Ping 
{
  public:
    typedef struct Acoustic_PingSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef Acoustic_PingTypeSupport TypeSupport;
    typedef Acoustic_PingDataWriter DataWriter;
    typedef Acoustic_PingDataReader DataReader;
    #endif

    DDS_Char *   sender ;
    DDS_Char *   receiver ;
    DDS_Long   timestamp ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Acoustic_Ping_get_typecode(void); /* Type code */

DDS_SEQUENCE(Acoustic_PingSeq, Acoustic_Ping);                                        

NDDSUSERDllExport
RTIBool Acoustic_Ping_initialize(
    Acoustic_Ping* self);

NDDSUSERDllExport
RTIBool Acoustic_Ping_initialize_ex(
    Acoustic_Ping* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Acoustic_Ping_initialize_w_params(
    Acoustic_Ping* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void Acoustic_Ping_finalize(
    Acoustic_Ping* self);

NDDSUSERDllExport
void Acoustic_Ping_finalize_ex(
    Acoustic_Ping* self,RTIBool deletePointers);

NDDSUSERDllExport
void Acoustic_Ping_finalize_w_params(
    Acoustic_Ping* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Acoustic_Ping_finalize_optional_members(
    Acoustic_Ping* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Acoustic_Ping_copy(
    Acoustic_Ping* dst,
    const Acoustic_Ping* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* Ping */

