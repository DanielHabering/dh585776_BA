

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Ping.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#ifndef dds_c_log_impl_h              
#include "dds_c/dds_c_log_impl.h"                                
#endif        

#ifndef cdr_type_h
#include "cdr/cdr_type.h"
#endif    

#ifndef osapi_heap_h
#include "osapi/osapi_heap.h" 
#endif
#else
#include "ndds_standalone_type.h"
#endif

#include "Ping.h"

/* ========================================================================= */
const char *Acoustic_PingTYPENAME = "Acoustic::Ping";

DDS_TypeCode* Acoustic_Ping_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode Acoustic_Ping_g_tc_sender_string = DDS_INITIALIZE_STRING_TYPECODE((64));
    static DDS_TypeCode Acoustic_Ping_g_tc_receiver_string = DDS_INITIALIZE_STRING_TYPECODE((64));
    static DDS_TypeCode_Member Acoustic_Ping_g_tc_members[3]=
    {

        {
            (char *)"sender",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"receiver",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"timestamp",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode Acoustic_Ping_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"Acoustic::Ping", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            3, /* Number of members */
            Acoustic_Ping_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for Acoustic_Ping*/

    if (is_initialized) {
        return &Acoustic_Ping_g_tc;
    }

    Acoustic_Ping_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&Acoustic_Ping_g_tc_sender_string;

    Acoustic_Ping_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&Acoustic_Ping_g_tc_receiver_string;

    Acoustic_Ping_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

    is_initialized = RTI_TRUE;

    return &Acoustic_Ping_g_tc;
}

RTIBool Acoustic_Ping_initialize(
    Acoustic_Ping* sample) {
    return Acoustic_Ping_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool Acoustic_Ping_initialize_ex(
    Acoustic_Ping* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return Acoustic_Ping_initialize_w_params(
        sample,&allocParams);

}

RTIBool Acoustic_Ping_initialize_w_params(
    Acoustic_Ping* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (allocParams) {} /* To avoid warnings */

    if (allocParams->allocate_memory){
        sample->sender= DDS_String_alloc ((64));
        if (sample->sender == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->sender!= NULL) { 
            sample->sender[0] = '\0';
        }
    }

    if (allocParams->allocate_memory){
        sample->receiver= DDS_String_alloc ((64));
        if (sample->receiver == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->receiver!= NULL) { 
            sample->receiver[0] = '\0';
        }
    }

    if (!RTICdrType_initLong(&sample->timestamp)) {
        return RTI_FALSE;
    }     

    return RTI_TRUE;
}

void Acoustic_Ping_finalize(
    Acoustic_Ping* sample)
{

    Acoustic_Ping_finalize_ex(sample,RTI_TRUE);
}

void Acoustic_Ping_finalize_ex(
    Acoustic_Ping* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    Acoustic_Ping_finalize_w_params(
        sample,&deallocParams);
}

void Acoustic_Ping_finalize_w_params(
    Acoustic_Ping* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }
    if (deallocParams) {} /* To avoid warnings */

    if (sample->sender != NULL) {
        DDS_String_free(sample->sender);
        sample->sender=NULL;

    }
    if (sample->receiver != NULL) {
        DDS_String_free(sample->receiver);
        sample->receiver=NULL;

    }

}

void Acoustic_Ping_finalize_optional_members(
    Acoustic_Ping* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool Acoustic_Ping_copy(
    Acoustic_Ping* dst,
    const Acoustic_Ping* src)
{

    if (!RTICdrType_copyStringEx (
        &dst->sender, src->sender, 
        (64) + 1, RTI_FALSE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->receiver, src->receiver, 
        (64) + 1, RTI_FALSE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyLong (
        &dst->timestamp, &src->timestamp)) { 
        return RTI_FALSE;
    }

    return RTI_TRUE;
}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'Acoustic_Ping' sequence class.
*/
#define T Acoustic_Ping
#define TSeq Acoustic_PingSeq
#define T_initialize_w_params Acoustic_Ping_initialize_w_params
#define T_finalize_w_params   Acoustic_Ping_finalize_w_params
#define T_copy       Acoustic_Ping_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#include "dds_cpp/generic/dds_cpp_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#include "dds_cpp_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params
#undef T_initialize_w_params
#undef TSeq
#undef T

