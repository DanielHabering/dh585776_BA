#include <stdio.h>
#include <stdlib.h>
#include "ndds/ndds_cpp.h"
#include "Ping.h"
#include "PingSupport.h"

typedef void (*PingCallback) (Acoustic_Ping* instance);

extern "C" int PingSubscriber_init(int domainId, char* topicName, PingCallback pingPointer);
