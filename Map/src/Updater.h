#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <math.h>
#include <vector>
#include "Geometry.h"

struct Point3d{
	double x;
	double y;
	double z;

};
typedef std::vector<bool> dim1;
typedef std::vector<dim1> dim2;
typedef std::vector<dim2> Matrix3d;


void initUpdater(double width, int x, int y, int z);
void setVoxelWidth(double val);
void addRay(double apuX1,double apuY1,double apuZ1,double apuX2, double apuY2, double apuZ2, int qual);
void addPoint(double apuX1,double apuY1,double apuZ1,double apuX2, double apuY2, double apuZ2, int qual,double rad);
