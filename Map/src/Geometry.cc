#include <vector>
#include "Geometry.h"
#include <algorithm>

using namespace std;

/**
* @brief Calculates the Distance between two octomap points
*
* @param p1 First Point as octomap::point3d
* @param p2 Second Point as octomal::point3d
*
* @return Distance as double
*/
inline double getDistance(const octomap::point3d &p1,const octomap::point3d &p2)
{
	double diffX = p1.x() - p2.x();
	double diffY = p1.y() - p2.y();
	double diffZ = p1.z() - p2.z();

	double dist = diffX*diffX+diffY*diffY+diffZ*diffZ;
	
	return sqrt(dist);
}

inline double getDistance(octomap::point3d* &p1,vector<double> &p2)
{
  
	double diffX = p1->x() - p2[0];
	double diffY = p1->y() - p2[1];
	double diffZ = p1->z() - p2[2];

	double dist = diffX*diffX+diffY*diffY+diffZ*diffZ;
	
	return sqrt(dist);
}

/**
* @brief Calculates the Distance between two points, represented by a 3-dimensional vector
*
* @param p1 First point as vector<double>(3)
* @param p2 Second point as vector<double>(3)
*
* @return Distance as double
*/
inline double getDistance(const vector<double> &p1,const vector<double> &p2)
{
	double dist = 0;
	for(int i = 0; i < 3; i++){
		dist += (p1[i]-p2[i])*(p1[i]-p2[i]);
	}
	dist  =  sqrt(dist);
	return dist;
}



/**
* @brief Checks which of two points A,B is closer to a given point min
*
* @param min Reference Point, to which the distance is checked
* @param A First point, 
* @param B
*
* @return 
*/
vector<double> getNearestVert(const octomap::point3d &min,const octomap::point3d &A,const octomap::point3d &B)
{
	vector<double> ret(3);
	if(getDistance(min,A)<getDistance(min,B)) {
		ret[0] = A.x();
		ret[1] = A.y();
		ret[2] = A.z();
	}
	else {
		ret[0] = B.x();
		ret[1] = B.y();
		ret[2] = B.z();
	}
	return ret;

}

/**
* @brief Calculates the crossproduct out of two given vectors
*
* @param a 3-dimensional vector
* @param b 3-dimensional vector
*
* @return A vector, containing the crossproduct of a and b
*/
vector<double> kreuzprodukt(const vector<double> &a,const vector<double> &b)
{
	vector<double> res(3);
	res[0] = a[1]*b[2]-a[2]*b[1];
	res[1] = a[2]*b[0]-a[0]*b[2];
	res[2] = a[0]*b[1]-a[1]*b[0];
	return res;
}


/**
* @brief Prints the content of a 8x3 matrix
*
* @param A The Matrix to be printed
*/
void print(const vector< vector<double> > &A) {
    int n = A.size();
    for (int i=0; i<8; i++) {
        for (int j=0; j<3; j++) {	
		cout<< A[i][j] << " ";
        }
        cout << "\n";
    }
    cout << endl;
}


/**
* @brief Prints the content of a 1xn vector
*
* @param a The vector to be printed
*/
void print(const vector<double> &a)
{
	std::cout << std::endl <<  "Vector: ";	
	for(int i = 0; i<a.size(); i++){
		std::cout << a[i] << " ";
	}
	std::cout << std::endl;
}

/**
* @brief Calculates the dot product of two 1xn vectors
*
* @param n First vector<double>
* @param m Second vector<double>
*
* @return -1 if the vectors have a different length, the dot product of the two vectors otherwise
*/
double scalar(const vector<double> &n,const vector<double> &m){
	if(n.size() != m.size()){
		std::cout << "Tried calculating the scalar of two different sized vectors\n";
		return -1;
	}
	
	else{
		double x = 0;
		for(int i = 0; i<n.size(); i++){
			x += n[i] * m[i];
		}
		return x;
	}
}


/**
* @brief Calculates the Euclidian Distance of a 1x3 vector p
*
* @param p  The vector<double> of which the distance shall be calculated
*
* @return the length as double
*/
double vectorLength(const vector<double> &p)
{
	double res = 0;
	for(int i = 0 ; i<3; i++){
		res += p[i]*p[i];
	}
	return sqrt(res);
}



/**
* @brief Checks if one of the points, given by a 8x3 matrix, is inside a sphere with radius r around point p
*
* @param M 8x3 matrix containing 8 points 
* @param p The middlepoint of the sphere
* @param r Radius of the sphere
*
* @return 
*/
bool pointProjIntersection(const vector<vector<double>> &M,const vector<double> &p, double r)
{

	vector<double> x(3);
	for(int i = 0; i<8; i++){
		for(int j = 0; j<3; j++){
			x[j] = M[i][j];
		}
		if(getDistance(x,p)<=r){
		 	return true;
		}
	}
	return false;
}


/**
* @brief returns the square of a double
*
* @param a Number to be squared
*
* @return Square of the given number
*/
double square(double a)
{
	return a*a;
}


/**
* @brief Calculates the distance of a line segment and a point
*
* @param p1 Starting point of the line segment
* @param p2 End point of the line segment
* @param q Point for which we will calculate the distance to the line segment
*
* @return Distance of q to the linesegment from p1 to p2
*/
double linePointDis(const vector<double> &p1,const vector<double> &p2,const vector<double> &q)
{
	
	vector<double> v(3);
	for(int i = 0; i<3; i++){
		v[i] = p2[i]-p1[i];
	}
	
	vector<double> w(3);
	for(int i = 0; i<3; i++){
		w[i] = q[i]-p1[i];
	}

	double c1 = scalar(w,v);
	if(c1 <= 0)
		return getDistance(q,p1);
	
	double c2 = scalar(v,v);
	if(c2 <= c1)
		return getDistance(q,p2);
	
	double b = c1 / c2;
	vector<double> Pb(3);
	for(int i = 0; i < 3; i++){
		Pb[i] = p1[i] + b*v[i];
	}
	
	return getDistance(q, Pb);
}

/**
* @brief Returns true, if one of the edges of the projected cube, denoted by a 8x3 matrix containing the projections of the vertices of the cube, intersects with the circle with radius r on the projection plane
*
* @param M 8x3 Matrix, containing 8 3-dimensional vectors, denoting the projected vertices of the cube
* @param p Middlepoint of the ground plane of the cone
* @param r Radius of the Circle around the middlepoint in the projection plane
*
* @return true, if one edge of the projected cube intersects with the circle around p with radius r
*/
bool edgeProjIntersection(const vector<vector<double>> &M,const vector<double> &p,double r)
{
	
	for (int i = 0; i < 12; ++i)
    {
	/*
	*  calculates the start and endpoint of every edge of the original cube
	*/
        const int axis = i >> 2;
	const int temp = i & 3;
	const int va = (1<<temp)-(temp==3);
	const int vb = va^(1<<axis);
	if(linePointDis(M[va],M[vb],p)<=r){
		
		return true;
	}
    }
	return false;
}

/**
* @brief Returns true, if the given point n lies inside the cube, defined by its minimal and maximal vertice
*
* @param n Point to be checked if it lays inside the cube
* @param min Vertice of the cube with minimal coordinates
* @param max Vertice of the cube with maximal coordinates
*
* @return True, if the point n lies inside the cube
*/
bool pointInVoxel(const vector<double> &n, octomap::point3d* min, octomap::point3d* max)
{
	if(n[0] < min->x() || n[0] > max->x())return false;
	if(n[1] < min->y() || n[1] > max->y())return false;
	if(n[2] < min->z() || n[2] > max->z())return false;
	return true;
}


/**
* @brief Returns the given vector multiplicated with an parameter p
*
* @param v 3x1 vector to be multiplicated with an parameter
* @param p Parameter as double
*
* @return p*v
*/
vector<double> parameter(vector<double> &v,double p){
	
	for(int i = 0; i < 3; ++i){
		v[i]=p*v[i];
	}
	return v;
}


/**
* @brief Return the closest point on the surface of a cube to a given point p
*
* @param min Vertice of the cube with minimal coordinates
* @param max Vertice of the cube with maximal coordinates
* @param p Point of which we want to calculate the closest point on the cube
*
* @return The closest point in the cube relativ to p
*/
vector<double> getClamp(octomap::point3d* min, octomap::point3d* max, vector<double> &p)
{
	vector<double> ret(3);
	for(int i = 0; i < 3; ++i){
		ret[i]=p[i];
	}
	
	if(ret[0] < min->x()) ret[0] = min->x();
	else if(ret[0] > max->x()) ret[0] = max->x();
	
	if(ret[1] < min->y()) ret[1] = min->y();
	else if(ret[1] > max->y()) ret[1] = max->y();

	if(ret[2] < min->z()) ret[2] = min->z();
	else if(ret[2] > max->z()) ret[2] = max->z();

	return ret;

}


bool planeIntersection(const vector<double> a,const vector<double> b,const vector<double> x,const vector<double> &m,const double &radius,const vector<double> &n)
{	
	
	//std::cout << "Testing Plane.... \n";
	//print(a);
	//print(b);
	//print(x);


      	
	double q = scalar(n,m)-scalar(n,x);
//	std::cout << "q: " << q << std::endl;
	
	double lambda = scalar(a,n);
//	std::cout << "lambda: " << lambda << std::endl;
	
	double my = scalar(b,n);
//	std::cout << "my: " << my << std::endl;

	double p = 0;
	
	
	if(lambda != 0){
	    q = q/lambda;
//	    std::cout << "q: " << q << std::endl;
	
	     p = -1*my/lambda;
	}
	else{
	  if(my == 0) return false;
	  p = my;
	  q = q/p;
	}
	
	
//	std::cout << "p: " << p << std::endl;
 
	double T = 0;
	
	vector<double> l(3);
	vector<double> k(3);

	bool setK = false;

	if(lambda != 0){
	    T = (-1*q)/p;
	
	    if(T >= 0 && T <= 1) {
	    	setK = true;
	//	std::cout << "Case: Lambda = voxelWidth, My = " << T << std::endl;
	
	    	for (int i = 0; i < 3; i++){
			k[i] = x[i] + (q+p*T)*a[i]+T*b[i];
	    	}
	    }

	    T = (1-q)/p;

	    if(T >= 0 && T <= 1){
	//	std::cout << "Case: Lamda = 0, My = " << T << std::endl;
	    	if(setK == false){
			setK = true;
    			 for (int i = 0; i < 3; i++){
        	                k[i] = x[i] + (q+p*T)*a[i]+T*b[i];
	                }
  
		}
		else{
			
	    		for(int i = 0; i < 3; i++){
				l[i] = x[i] +(q+p*T)*a[i]+T*b[i];
	    		}
		}
	    }

	    if(q >= 0 && q <= 1){
	//	std::cout << "Case: Lambda = " << q << ", My = 0" << std::endl;
		if(setK == false){
			setK = true;
			
			for(int i = 0; i < 3; i++){
				k[i] = x[i] + q*a[i];
			}
		
		}
		else{
			for(int i = 0; i < 3; i++){
				l[i] = x[i] + q*a[i];
			}
		}
	    }

	    if(q+p >= 0 && q+p <= 1){
	//	std::cout << "Case: Lambda = " << q+p << ", My = 1" << std::endl;
		if(setK == false){
			setK = true;
			for(int i = 0; i < 3; i++) {
				k[i] = x[i] + (q+p)*a[i] + b[i];
			}
		}
		else{
			for(int i = 0; i < 3; i++){
				l[i] = x[i] + (q+p)*a[i] + b[i];
			}
		}
	    }
	}
	
	else{
	  if(q < 0 || q > 1) return false;
	  
	  for (int i = 0; i < 3; i++){
		k[i] = x[i]+ q*b[i];
	    }
	  print(k);
	  
	  
	  for(int i = 0; i < 3; i++){
		l[i] = x[i]+a[i]+q*b[i];
	    }
	  print(l);
	  
	}
	
	
	if(linePointDis(k,l,m)<radius){
	    std::cout << "Plane Intersected \n";
	    return true;
	}
	else return false;
	

}

vector<double> getClosestPoint(octomap::point3d* min, double voxelWidth, vector<double> &p)
{
	vector<double> ret(3);
	
	vector<double> temp(3);
	
	double minDis = getDistance(min,p);
	
	for(int i = 0; i < 8; i++){
		
		temp[0] = min->x()+(i&1)*voxelWidth;
		temp[1] = min->y()+((i&2)>>1)*voxelWidth;
		temp[2] = min->z()+((i&4)>>2)*voxelWidth;
	
		double dis = getDistance(temp,p);
		if(dis <= minDis) {
		    minDis = dis;
		    ret = temp;
		}
	}	

	return ret;
}	

bool rayBoxIntersection(octomap::point3d* min, octomap::point3d* max, vector<double> near, vector<double> n,double voxelWidth)
{
    vector<double> dirfrac(3);
    
    for(int i = 0; i < 3; i++){
	dirfrac[i] = 1/n[i];
    }
     
    vector<double> minT(3);
      minT[0] = (min->x() - near[0])*dirfrac[0];  
      minT[1] = (min->y() - near[1])*dirfrac[1]; 
      minT[2] = (min->z() - near[2])*dirfrac[2];
      
    vector<double> maxT(3);
      maxT[0] = (max->x() - near[0])*dirfrac[0];
      maxT[1] = (max->y() - near[1])*dirfrac[1];
      maxT[2] = (max->z() - near[2])*dirfrac[2];
      
    double tmin = std::max(std::max(std::min(minT[0],maxT[0]),std::min(minT[1],maxT[1])),std::min(minT[2],maxT[2]));
    double tmax = std::min(std::min(std::max(minT[0],maxT[0]),std::max(minT[1],maxT[1])),std::max(minT[2],maxT[2]));
    
  
    
    if(tmax < 0) return false;
    if(tmin>tmax) return false;
    
    return true;
    
    
    
    
}


/**
* @brief Returns true if the given cube is intersected or inside the double cone, defined by its endpoints A and B and its maximal radius 
*
* @param min Vertice of the cube with minimal coordinates
* @param max Vertice of the cube with maximal coordinates
* @param A Starting point of the double cone
* @param B Endpoint of the double cone
* @param radius Radius of the cones exactly in the middle between A and B
*
* @return True, if the cube is inside or intersected by the double cone.  False otherwise
*/
bool boxConeIntersection(octomap::point3d* min, octomap::point3d* max, octomap::point3d* A, octomap::point3d* B, double radius)
{
  
  
	//std::cout << "Check Voxel: " << min->x() << " " <<  min->y() << " " << min->z() << "\n";
	double voxelWidth = max->x()-min->x();

	vector<double> near = getNearestVert(*min,*A,*B);
  
	
	

	vector<double> n(3);
	vector<double> z(3);
	vector<double> far(3);

	if(near[0] == A->x()&& near[1] == A->y() && near[2] == A->z()){
		n[0] = B->x()-A->x();
		n[1] = B->y()-A->y();
		n[2] = B->z()-A->z();
		far[0] = B->x();
		far[1] = B->y();
		far[2] = B->z();
		z[0] = A->x()-B->x();
                z[1] = A->y()-B->y();
                z[2] = A->z()-B->z();


	}
	else{
		n[0] = A->x()-B->x();
		n[1] = A->y()-B->y();
		n[2] = A->z()-B->z();
		far[0] = A->x();
		far[1] = A->y();
		far[2] = A->z();
		z[0] = B->x()-A->x();
                z[1] = B->y()-A->y();
                z[2] = B->z()-A->z();


	} 


	
	vector<double> p=parameter(n,0.5);
	vector<double> q=parameter(z,0.5);
	vector<double> m = p;
	for(int i = 0; i < 3 ; i++){
	    m[i] += near[i];
	}
	vector<vector<double>> M(8,vector<double>(3));
	vector<double> x(3);

	double t = 0.0;
	bool facesTested = false;
	/*
	* Calculates the projection of the vertices of the cube onto the plane which is exactly in the middle between A and B
	* A 8x3 Matrix is filled with the projections
	*/	
	
 	for(int i = 0; i < 8; i++){
 		
 		x[0] = min->x()-near[0]+(i&1)*voxelWidth;
 		x[1] = min->y()-near[1]+((i&2)>>1)*voxelWidth;
 		x[2] = min->z()-near[2]+((i&4)>>2)*voxelWidth;
 
 		t = scalar(p,n)/scalar(n,x);
 
 		if(t < 0) return false;
 		
 		/*
 		*  If t<1, then the i-th vertice of the cube lies on the other side of the plane. Therefore the cube is intersected by the plane
 		*  So we have to take the other Endpoint of the cone as projection basis.  
 		*/
 		if(t < 1 ){
		//	std::cout << "Vertex on wrong side of Plane \n";
 			
 			
 			x[0] = min->x()-far[0]+(i&1)*voxelWidth;
 			x[1] = min->y()-far[1]+((i&2)>>1)*voxelWidth;	
 			x[2] = min->z()-far[2]+((i&4)>>2)*voxelWidth;
 			
 			t = scalar(q,z)/scalar(z,x);
 			for(int j = 0; j < 3; j++){
                                 M[i][j] = far[j] + t*x[j];
                       	}
 			
 			if(!facesTested){
			      facesTested = true;
	    
			      
			    vector<double> closestPoint = getClosestPoint(min,voxelWidth,m);
			    
		//	    std::cout << "Check for Faces of "<< min->x() << " " << min->y() << " " << min->z() << std::endl;
			    
			    vector<double> a(3);
			    vector<double> b(3);
			    
			    for(int i = 0; i<3; i++){
				a[i] = 0;
				b[i] = 0;
			    }
			    
			    /*
			    * Face in x/y direction 
			    */
			    if(min->x() - closestPoint[0] == 0) a[0] = voxelWidth;
			    else a[0] = -1*voxelWidth;
			    
			    if(min->y() - closestPoint[1] == 0) b[1] = voxelWidth;
			    else b[1] = -1*voxelWidth;
			    
			    
				
			//	if(planeIntersection(a,b,closestPoint,m,radius,n)) return true;
			    
			    /*
			    * Face in y/z plane
			    */
			    a[0] = 0;
			    b[1] = 0;
			    
			    if(min->y() - closestPoint[1] == 0) a[1] = voxelWidth;
			    else a[1] = -1*voxelWidth;
			    
			    if(min->z() - closestPoint[2] == 0) b[2] = voxelWidth;
			    else b[2] = -1*voxelWidth;
			    
			//	if(planeIntersection(a,b,closestPoint,m,radius,n)) return true;
			    
			    /*
			    * face in x/z plane
			    */
				    
			    a[1] = 0;
			    b[2] = 0;
			    
			    if(min->x() - closestPoint[0] == 0) a[0] = voxelWidth;
			    else a[0] = -1*voxelWidth;
			    
			    if(min->z() - closestPoint[2] == 0) b[2] = voxelWidth;
			    else b[2] = -1*voxelWidth;
			    
			//	if(planeIntersection(a,b,closestPoint,m,radius,n)) return true;
 			}
 
 		}
 		else{
 			for(int j = 0; j < 3; j++){
            		     	M[i][j] = near[j] + t*x[j];
 	        	}
 		}
 			
 	}

	/*
	*  If one of the projected points on the projection plane is closer to the middlepoint p than the given maximal radius, then the cibe is intersected or inside the double         *  cone
	*/
	if(pointProjIntersection(M,m,radius)) return true;
	
	/*
	*  If the distance of one of the projected edges of the cube to the middlepoint q is  smaller or equal to the maximal radius, than the edge is intersected by the double c        *  one, and so is the cone.
	*/
	if(edgeProjIntersection(M,m,radius)) return true;
	
	
	if(rayBoxIntersection(min,max,near,n,voxelWidth)) {
	  std::cout << "voxel on axis \n";
	  return true; 
	}
	
	
	/*
        *  If the Middle point of the doublecone lies inside the cube, than its intersected or inside the double cone
        */
  
	if(pointInVoxel(near,min,max)){
	    std::cout << "Endpoint in Voxel \n";
	    return true;
	}


	return false;

	 	 	
} 
