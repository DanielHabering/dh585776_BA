#include <stdio.h>
#include <stdlib.h>
#include "Updater.h"
#include <octomap/octomap.h>
#include <octomap/OcTree.h>
#include <map>
#include <iostream>
#include <list>
#include <vector>
#include "Geometry.h"

using namespace std;
/**
* @brief The Octree, representing our map
*
* @param 0.5 - size of smallest possible voxel size
*/
octomap::OcTree tree(1);


/**
* @brief Maximal radius of the two cones, building the body that represents
*        the space in which we have an information of the occurence of obstacles
*/
double radius;


/**
* @brief Boolean matrix for keeping track of every checked voxel
*/
Matrix3d tested;


/**
* @brief minimum voxel size (size of an edge)
*/
double voxelWidth;

int counter;
/**
* @brief list of voxels that maybe inside or intersected by the doublecone
*/
std::list<octomap::OcTreeKey> toBeTested;

int widthX = 100;
int widthY = 100;
int depthZ = 100;


void initMatrix()
{
	int x,y,z;
	for(x = 0; x < widthX/voxelWidth;x++){
	  for(y=0; y<widthY/voxelWidth;y++){
	     for(z=0;z<depthZ/voxelWidth;z++){
	    	tested[x][y][z]=false;
	     }
	  }
	}
}

/**
* @brief initializes the updater for the map
*
* @param width: minimum width of a voxel
* @param rad: maximum radius of the doublecone
*/
void initUpdater(double width, int x, int y, int z){
	widthX=x;
	widthY=y;
	depthZ=z;
	setVoxelWidth(width);
	octomap::OcTree tmp(voxelWidth);
	Matrix3d temp(widthX/voxelWidth,dim2(widthY/voxelWidth,dim1(depthZ/voxelWidth)));
	std::cout << "Matrix initialized with: " << widthX/voxelWidth << " " << widthY/voxelWidth << " " << depthZ/voxelWidth << std::endl;
	tested=temp;
}


/**
* @brief Sets the minimum voxel size
*
* @param val: size of the voxel
*/
void setVoxelWidth(double val)
{
	voxelWidth=val;
}


/**
* @brief Initializes an octomap::point3d, given three coordinates
*
* @param x: X-coordinate of the point
* @param y: Y-coordinate of the point
* @param z: Z-coordinate of the point
*
* @return the initialized point
*/
octomap::point3d initPoint(double x,double y, double z)
{

	octomap::point3d res;
	res.x() = x;
	res.y() = y;
	res.z() = z;
	return res;
}





/**
* @brief returns the voxel to the right of the voxel containing a given point
*
* @param p: Point inside a voxel
*
* @return The Key identifying the right neighbour of the voxel, which contains the given point
*/
octomap::OcTreeKey getRight(const octomap::point3d &p)
{
	octomap::point3d res;
	res.x()=p.x();
	res.y()=p.y();
	res.z()=p.z();
	res.x()+=voxelWidth;
	return tree.coordToKey(res);
}
/**
* @brief returns the voxel to the left of the voxel containing a given point
*
* @param p: Point inside a voxel
*
* @return The Key identifying the left neighbour of the voxel, which contains the given point
*/
octomap::OcTreeKey getLeft(const octomap::point3d &p)
{
	octomap::point3d res;
	res.x()=p.x();
	res.y()=p.y();
	res.z()=p.z();
	
	res.x() -= voxelWidth;
	return tree.coordToKey(res);
}
/**
* @brief returns the voxel above of the voxel containing a given point
*
* @param p: Point inside a voxel
*
* @return The Key identifying the upper neighbour of the voxel, which contains the given point
*/
octomap::OcTreeKey getUpper(const octomap::point3d &p)
{
	octomap::point3d res;
	res.x()=p.x();
	res.y()=p.y();
	res.z()=p.z();
	res.z() += voxelWidth; 
	return tree.coordToKey(res);
}
/**
* @brief returns the voxel below the voxel containing a given point
*
* @param p: Point inside a voxel
*
* @return The Key identifying the lower neighbour of the voxel, which contains the given point
*/
octomap::OcTreeKey getLower(const octomap::point3d &p)
{
		octomap::point3d res;
	res.x()=p.x();
	res.y()=p.y();
	res.z()=p.z();
	res.z() -= voxelWidth;
	return tree.coordToKey(res);
}
/**
* @brief returns the voxel in the front of the voxel containing a given point
*
* @param p: Point inside a voxel
*
* @return The Key identifying the front neighbour of the voxel, which contains the given point
*/
octomap::OcTreeKey getFront(const octomap::point3d &p)
{
		octomap::point3d res;
	res.x()=p.x();
	res.y()=p.y();
	res.z()=p.z();
	res.y() += voxelWidth;
	return tree.coordToKey(res);
}
/**
* @brief returns the voxel behind the voxel containing a given point
*
* @param p: Point inside a voxel
*
* @return The Key identifying the behind neighbour of the voxel, which contains the given point
*/
octomap::OcTreeKey getBack(const octomap::point3d &p)
{
	octomap::point3d res;
	res.x()=p.x();
	res.y()=p.y();
	res.z()=p.z();
	res.y() -= voxelWidth;
	return tree.coordToKey(res);
}


/**
* @brief Checks if point lays inside the boundaries of our world (100x100x60)
*
* @param p: The point to be checked
*
* @return 0 if the point lays inside, 1 otherwise
*/
bool inBound (const octomap::point3d &p){
	if(p.x()<-1*(int)(widthX/2)||p.x()>(int)(widthX/2))return false;
	if(p.y()<-1*(int)(widthY/2)||p.y()>(int)(widthY/2))return false;
	if(p.z()<-1*(int)(depthZ/2)||p.z()>(int)(depthZ/2))return false;
	return true;
}


/**
* @brief writes a boolean value into the tested matrix, where the position is defined by the coordinates of the given point
*
* @param point: The point, for which the value is given.
* @param val: Boolean value denoting if the point is already checked
*/
void writeTested(const octomap::point3d &point,bool val)
{
	int x = (point.x()+(int)(widthX/2)-voxelWidth/2)/voxelWidth;
   int y = (point.y()+(int)(widthY/2)-voxelWidth/2)/voxelWidth;
   int z = (point.z()+(int)(depthZ/2)-voxelWidth/2)/voxelWidth;
	
	tested[x][y][z]=val;
}


/**
* @brief Reads a value out of the tested Matrix, where the position is defined by the coordinates of the given point
*
* @param point: The point, for which the value shall be read.
*
* @return Returns 0 if the point is not checked yet, 1 otherwise
*/
bool readTested(const octomap::point3d &point)
{
	
   int x = (int)((point.x()+(int)(widthX/2)-voxelWidth/2)/voxelWidth);
   int y = (int)((point.y()+(int)(widthY/2)-voxelWidth/2)/voxelWidth);
   int z = (int)((point.z()+(int)(depthZ/2)-voxelWidth/2)/voxelWidth);
  
   return tested[x][y][z];

}

/**
* @brief Returns the vertice with minimal coordinates
*
* @param node: The node of which voxel the minimal coordinates are wanted
*
* @return the coordinates of the minimal vertice
*/
octomap::point3d getMinPoint(const octomap::OcTreeKey &node)
{
	octomap::point3d min;
	min.x() = tree.keyToCoord(node).x()-voxelWidth/2;
	min.y() = tree.keyToCoord(node).y()-voxelWidth/2;
	min.z() = tree.keyToCoord(node).z()-voxelWidth/2;
	return min;
}

/**
* @brief Returns the vertice with maximmal coordinates
*
* @param node: The node of which voxel the maximal coordinates are wanted
*
* @return the coordinates of the maximal vertice
*/
octomap::point3d getMaxPoint(const octomap::OcTreeKey &node)
{
	octomap::point3d max;
	max.x() = tree.keyToCoord(node).x()+voxelWidth/2;
	max.y() = tree.keyToCoord(node).y()+voxelWidth/2;
	max.z() = tree.keyToCoord(node).z()+voxelWidth/2;
	return max;

}



/**
* @brief Checks if voxel, defined by its maximum and minimum vertice, is intersected by a sphere, defined by its middle point and radius
*
* @param minCorner: Vertice with minimal coordinates of the voxel
* @param maxCorner: Vertice with maximal coordinates of the voxel
* @param middlePoint: Middlepoint of the sphere
* @param r: Radius of the sphere
*
* @return 0 if the voxel is not intersected
*/
octomap::point3d getNearestPoint(octomap::point3d minCorner, octomap::point3d maxCorner, octomap::point3d middlePoint){
        octomap::point3d near=middlePoint;
		  if(near.x()<minCorner.x()) near.x()=minCorner.x();
		  else if(near.x()>maxCorner.x()) near.x() = maxCorner.x();
		
		  if(near.y()<minCorner.y()) near.y()=minCorner.y();
		  else if(near.y()>maxCorner.y()) near.y() = maxCorner.y();
	
		  if(near.z()<minCorner.z()) near.z()=minCorner.z();
		  else if(near.z()>maxCorner.z()) near.z() = maxCorner.z();

		  return near;
}

/**
* @brief Adds the neighbours of a voxel, defined by its middlepoint, to the list of possibel intersected voxels
*
* @param point: The middlepoint if the voxel
*/
void addNeighbours(octomap::point3d point)
{
	if(inBound(tree.keyToCoord(getRight(point)))){
		if(!readTested(tree.keyToCoord(getRight(point)))){
			toBeTested.push_back(getRight(point));
			writeTested(tree.keyToCoord(getRight(point)),true);
		}
	}
	if(inBound(tree.keyToCoord(getLeft(point)))){
		if(!readTested(tree.keyToCoord(getLeft(point)))){
			toBeTested.push_back(getLeft(point));
			writeTested(tree.keyToCoord(getLeft(point)),true);
		}
	}
	if(inBound(tree.keyToCoord(getUpper(point)))){
		if(!readTested(tree.keyToCoord(getUpper(point)))){
			toBeTested.push_back(getUpper(point));
			writeTested(tree.keyToCoord(getUpper(point)),true);
		}
	}
	if(inBound(tree.keyToCoord(getLower(point)))){
		if(!readTested(tree.keyToCoord(getLower(point)))){
			toBeTested.push_back(getLower(point));
			writeTested(tree.keyToCoord(getLower(point)),true);
		}
	}	
	if(inBound(tree.keyToCoord(getFront(point)))){
		if(!readTested(tree.keyToCoord(getFront(point)))){
			toBeTested.push_back(getFront(point));
			writeTested(tree.keyToCoord(getFront(point)),true);
		}
	}
	if(inBound(tree.keyToCoord(getBack(point)))){
		if(!readTested(tree.keyToCoord(getBack(point)))){
			toBeTested.push_back(getBack(point));
			writeTested(tree.keyToCoord(getBack(point)),true);
		}
	}
}


/**
* @brief Updates the tree, that represents our map, given a node and the information we got about this node
*
* @param node Node of the tree
* @param qual Integer, representing the qualitiy of our connection (1 = We have had an acoustic connection, 0 = We had no acoustic response)
*/
void update(octomap::OcTreeKey node, int qual){
	if(qual==1){
		tree.updateNode(node,true);
	}
	
	else if(qual == 0){
		tree.updateNode(node,false);
	}
}

/**
* @brief Checks if a voxel, defined by its nodekey, is intersected or inside the double cone, defined by the end and the start of its axis
*
* @param node: The node to be checked
* @param A: Start point of the axis of the doublecone
* @param B: End point of the axis of the doublecone
*/
void checkPoint(octomap::OcTreeKey node, octomap::point3d A,octomap::point3d B,int qual)
{
	octomap::point3d point = tree.keyToCoord(node);
	
       	octomap::point3d max = getMaxPoint(node);
	octomap::point3d min = getMinPoint(node);

	if(boxConeIntersection(&min,&max,&A,&B,radius)){
		addNeighbours(tree.keyToCoord(node));
		update(node,qual);
		counter++;
		return;	
	}
}


/**
* @brief Given a new Connection information between two APUs the map is updated
*
* @param apuX1: X-coordinate of the first APU
* @param apuY1: Y-coordinate of the first APU
* @param apuZ1: Z-coordinate of the first APU
* @param apuX2: X-coordinate of the second APU
* @param apuY2: Y-coordinate of the second APU
* @param apuZ2: Z-coordinate of the second APU
* @param qual: Type of information about the connection (Successfull, No connection or Weak connection)
*/
void addPoint(double apuX1,double apuY1,double apuZ1,double apuX2, double apuY2, double apuZ2, int qual,double rad)
{
	counter = 0;
	
	radius = rad;
	octomap::point3d apu1 = initPoint(apuX1,apuY1,apuZ1);
	octomap::point3d apu2 = initPoint(apuX2,apuY2,apuZ2);

	initMatrix();
	toBeTested.push_back(tree.coordToKey(apu1));
	
	if(!inBound(apu1) || !inBound((apu2))){
	    std::cout<<"Initial Point not in bound\n";
	    return;
	}
	
	
	writeTested(tree.keyToCoord(tree.coordToKey(apu1)),true);
	addNeighbours(tree.keyToCoord(tree.coordToKey(apu1)));
	while(!toBeTested.empty()){
		checkPoint(toBeTested.front(),apu1,apu2,qual);
		toBeTested.pop_front();
	}
	std::cout << "Counter: " << counter << std::endl;
	tree.writeBinary("mapmodule_test.bt");
}

void addRay(double apuX1, double apuY1, double apuZ1, double apuX2, double apuY2, double apuZ2, int qual)
{
	octomap::point3d apu1 = initPoint(apuX1,apuY1,apuZ1);
	octomap::point3d apu2 = initPoint(apuX2,apuY2,apuZ2);
	std::vector<octomap::point3d> ray;
	
	tree.computeRay(apu1,apu2,ray);
	
	for(int i = 0; i < ray.size(); i++){
	    update(tree.coordToKey(ray[i]),qual);
	}
	
	tree.writeBinary("mapmodule_test.bt");
	
}



