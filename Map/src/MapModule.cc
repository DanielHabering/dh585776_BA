#include <stdio.h>
#include <stdlib.h>
#include "../../RTI/Connection/Connection_subscriber.h"
#include "Updater.h"
#include "MapModule.h"
#include <chrono>	
#include <iostream>


double getDistance(double aX,double aY,double aZ, double bX, double bY, double bZ)
{
	double diffX = aX-bX;
	double diffY = aY-bY;
	double diffZ = aZ-bZ;

	double ret = diffX*diffX+diffY*diffY+diffZ*diffZ;
	ret = sqrt(ret);
	
	printf("Distance: %f \n",ret);
	return ret;

}

void updateMap(Mapping_Connection* instance){
  	double radius = getDistance(instance->apuA.x,instance->apuA.y,instance->apuA.z,instance->apuB.x,instance->apuB.y,instance->apuB.z);

	radius = 0.4*radius;
	int qual;
	if(instance->qual==CONNECTION)
	{
		qual=0;
	        addRay(instance->apuA.x,instance->apuA.y,instance->apuA.z,instance->apuB.x,instance->apuB.y,instance->apuB.z,qual);
	}
	else {
	  if(instance->qual==NO_CONNECTION){
	      qual=1;
	      std::cout << "Add Cone: " << instance->apuA.x << " " << instance->apuA.y << " " << instance->apuA.z << " --> " << instance->apuB.x << " " << instance->apuB.y << " " << instance->apuB.z << " - " <<qual << " " << radius << std::endl;
	      addPoint(instance->apuA.x,instance->apuA.y,instance->apuA.z,instance->apuB.x,instance->apuB.y,instance->apuB.z,qual,radius);
	  }
	}

}

int main(){
	
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	initUpdater(1,50,50,250);
	//addPoint(-20,20,-113,12.5,3.2,-10.5,1,getDistance(-19.5,-4.5,-20,12.5,3.2,-10.5)*0.4);

	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
	std::cout << "Duration: " << duration << std::endl;
        ConnectionSubscriber_init(1,"Connection",&updateMap);
	while(1){
	}	
	return 1;
}
