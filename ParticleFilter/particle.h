#include <stdio.h>

struct Position{
	double x;
	double y;
	double z;
} 

struct Particle{
	Position pos;
	double weight;
}
