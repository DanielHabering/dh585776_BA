#include <stdlib.h>
#include <stdio.h>
#include "../../RTI/Connection/Connection_publisher.h"
#include "Position.h"
#include <map>
#include <vector>
void addConnection(int id2, long int time);
void addInterrupt(int id2);
void addPosition(const std::vector<std::pair<pos3d,double>> &particleSet, int id);
void updateMap();
void initMappingInterface(int id,int max);