#include "PositionModule.h"
#include "MeltingModel.h"
#include "AcousticModel.h"
#include "../../RTI/MeltingRequest/MeltingRequest_subscriber.h"
#include "../../RTI/LocatingSignal/LocatingSignal_subscriber.h"
#include "../../RTI/LocatingSignal/LocatingSignal_publisher.h"
#include "../../RTI/LocatingSignal/LocatingSignalPlugin.h"
#include "../../RTI/Ping/Ping_publisher.h"
#include "../../RTI/Ping/Ping_subscriber.h"
#include "../../RTI/Localization/Localization_subscriber.h"
#include "../../RTI/Request/PositionRequest_publisher.h"
#include "../../RTI/MeltReturn/Return_subscriber.h"
#include "../../RTI/AcousticRuntime/AcousticRuntime_subscriber.h"
#include "../../RTI/RuntimeRequest/RuntimeRequest_publisher.h"



using namespace std;
struct timeval tp;

char* apu_id;
int id;

int maxApu = 12;
int count = 0;

ParticleSet partSet;

vector<bool> Answered;

//For testing purposes
vector<pos3d> ApuPositions;


long long int actTravelTime = 0;
/**
* @brief Initializes a boolean vector with false in every Place
*
* @param vec: Vector to be initialized
*/
void initBoolVector(vector<bool>& vec){
    for(int i = 0; i < vec.size(); i++){
	vec[i]=false;
    }
}


  

/**
* @brief Resizes the weights in the Particle Set, such that the weights equals 1 in addition
*/
void normalize(){
    double total = 0;
    for(int i = 0; i< partSet.set.size(); i++){
	total+= partSet.set[i].weight;
    }
  
    for(int i = 0; i < partSet.set.size(); i++){
	partSet.set[i].weight = partSet.set[i].weight/total;
    }
    
}

std::vector<std::pair<pos3d,double> > getPairArray(const Acoustic_ParticleSeq &particleSeq, int size){
    std::vector< std::pair<pos3d,double> > ret(size);
    for(int i = 0; i < size; i++){
	pair<pos3d,double> p;
	pos3d pos;
	pos.x = particleSeq[i].x;
	pos.y = particleSeq[i].y;
	pos.z = particleSeq[i].z;
	p = std::make_pair(pos,particleSeq[i].w);
	
	ret[i]=p;
	     
    }
    
    return ret;
}

std::vector<std::pair<pos3d,double>> getPairArray(const ParticleSet &particleSeq){
    std::vector<std::pair<pos3d,double>> ret;
    for(int i = 0; i < particleSeq.size; i++){
	pos3d pos;
	pos.x = particleSeq.set[i].pos.x;
	pos.y = particleSeq.set[i].pos.y;
	pos.z = particleSeq.set[i].pos.z;
	
	pair<pos3d,double> p(pos,particleSeq.set[i].weight);
	ret.push_back(p);
    }
    
    return ret;
}


double getError(){
    double ret = 0;
    double temp = 0;

    pos3d real = ApuPositions[id];
    for(int i = 0; i < partSet.size; i++){
	temp = abs(partSet.set[i].pos.x-real.x)*partSet.set[i].weight;
	temp += abs(partSet.set[i].pos.y-real.y)*partSet.set[i].weight;
	temp += abs(partSet.set[i].pos.x-real.x)*partSet.set[i].weight;
	ret += temp;
    }
    ret = ret/(partSet.size-1);
    return sqrt(ret);
    
}

double entropy(){
  double ret = 0;
  pos3d bar = newPosition(0,0,0);
  double total = 0;

  for(int i = 0; i < partSet.size; i++){
      bar.x += partSet.set[i].pos.x*partSet.set[i].weight;
      bar.y += partSet.set[i].pos.y*partSet.set[i].weight;
      bar.z += partSet.set[i].pos.z*partSet.set[i].weight;
      total += partSet.set[i].weight;
  }
  
  for(int i = 0; i < partSet.size; i++){
      ret += partSet.set[i].weight * abs(partSet.set[i].pos.x-bar.x);
      ret += partSet.set[i].weight * abs(partSet.set[i].pos.y-bar.y);
      ret += partSet.set[i].weight * abs(partSet.set[i].pos.z-bar.z);
  }
  return sqrt(ret);
}

double entropy(const Acoustic_ParticleSeq& particleSeq, int size){
    double ret = 0;
    pos3d bar = newPosition(0,0,0);
    double total = 0;
   /* for(int i = 0; i < size; i++){
	ret += particleSeq[i].w*log(particleSeq[i].w);
    }
    ret = -1*ret;*/
    for(int i = 0; i < size; i++) {
	bar.x += particleSeq[i].x*particleSeq[i].w;
	bar.y += particleSeq[i].y*particleSeq[i].w;
	bar.z += particleSeq[i].z*particleSeq[i].w;
	total += particleSeq[i].w;
    }

    
    for(int i = 0; i < size; i++){
	ret += particleSeq[i].w * abs(particleSeq[i].x-bar.x);
	ret += particleSeq[i].w * abs(particleSeq[i].y-bar.y);
	ret += particleSeq[i].w * abs(particleSeq[i].z-bar.z);
    }
    ret = sqrt(ret);
   
    return ret;
}

/**
* @brief Prints the set of particles and their weight 
*/
void printAll(){
 
    for(int i = 0; i < partSet.size; i ++){
	addPoint(partSet.set[i].pos.x,partSet.set[i].pos.y,partSet.set[i].pos.z,partSet.set[i].weight);
    }
  
    addError(getError(),count);
    std::cout << "Plotting... with "<<id << " " << count << "\n";
     plot(id,count);
    
   
}

/**
* @brief Pings every known APU in Order to resize the weights of the Particles
* 	Waits for an Answer a given time and then pings the next APU
*       In the end all weights are normalized
*/
void verifyPosition(){
    struct timeval tp;
    char reciever[6];
    char integer_string[5];
    
    long int startTime=0;
    long int actTime = 0;
    

    for(int i = 0; i < maxApu; i++){
	  cout << "Wait for: " << i << endl;
	  if(i == id)continue;
	  
	  reciever[0] = '\0';
	  strcat(reciever,"apu-");
	  snprintf(integer_string,5,"%d",i);
	  strcat(reciever,integer_string);
	  
      
	  gettimeofday(&tp, NULL);
	  startTime = tp.tv_sec * 1000 + tp.tv_usec / 1000;
	  actTime = tp.tv_sec * 1000 + tp.tv_usec / 1000;
	  
	  Answered[i]=false;
	 PingWrite(apu_id,&(reciever[0]),startTime);
	  
	  long int exWaitTime = 1400;
	  while(startTime + exWaitTime >= actTime){
	    if(Answered[i]==true){
		std::cout << "Answer Received from: " << i << std::endl;
		break;
	    }
	     usleep(1000);
	     gettimeofday(&tp, NULL);
	     actTime = tp.tv_sec * 1000 + tp.tv_usec / 1000;
	  }
	  if(Answered[i]==false)addInterrupt(i);
	  
	  
    }
    normalize();
    int id1;
    int n = sscanf(apu_id,"apu-%d",&id1);
    addPosition(getPairArray(partSet),id1);
    count ++;
    printAll();
    updateMap();
}




/**
* @brief Initializes the particle set with a given size, a given position and an initial weight of 1/size
*
* @param pSet
* @param size
* @param pos
*/
void initParticleSet(int size,pos3d pos){
    partSet.set.resize(size);
    std::cout << "Size: " << partSet.set.size() << std::endl;
    partSet.size=size;
    for(int i = 0; i < size; i++){
	partSet.set[i].pos = pos;
	partSet.set[i].weight = (double)1/size; 
	partSet.totalWeight += 1/size;
    }
}


/**
* @brief Picks a particle according to its weight, impling that the weights are normalized
*
* @return The picked particle
*/
Particle pickParticle(){
    double total = 0;

    double p = (double)std::rand()/RAND_MAX;
       
    for(int i = 0; i < partSet.set.size(); i++){
	total += partSet.set[i].weight;
	if(p<= total){
	   
	    return partSet.set[i];
	}
    }
    
}


/**
* @brief Replaces the vector of Particles in our particle set with a given vector of particles
*
* @param next: The vector of new particles 
*/
void replaceVector(const vector<Particle>& next){
    for(int i = 0; i < partSet.set.size();i++){
	partSet.set[i]=next[i];
    }
}

/**
* @brief On recieving the message that the APU melted itself down, the positions of the particles
*	are adapted, according to the Melting Model. Then the APU tries to verify its position
*
* @param instance: The MeltRequest Message
*/
void relocationHandler(Melting_MeltRequest* instance){
 
    if(strcmp(instance->id,apu_id)!=0)return;
    cout << "\n Recieved Melting with depth: " << instance->depth << " and Speed: " << instance->speed << endl;
    vector<Particle> next(partSet.set.size());
    
    for(int i = 0; i < next.size(); i++){
	   
	   Particle picked =  pickParticle();
           next[i].pos = getNewPositionGauss(picked.pos,instance->depth,instance->speed);
	   next[i].weight = 0;
	   
    }
    
    replaceVector(next);
    
   // printAll();
    
    verifyPosition();
    
    //printAll();
}

/**
* @brief Given the id of an APU, the according entry of that APU is set to true in the Answered vector
*
* @param sender: ID of the APU from which a message was recieved
*/
void setAnswered(char* sender){
    int id;
    int n = sscanf(sender,"apu-%d",&id);
    Answered[id]=true;
}


void ReturnHandler(range_Return *instance){
   if(strcmp(instance->id,apu_id)!=0)return;
   double speed = 1;

   
   double depth = abs(instance->depth - ApuPositions[id].z);
    cout << "\n Recieved Melting with depth: " << depth << endl;
    vector<Particle> next(partSet.set.size());
    
    for(int i = 0; i < next.size(); i++){
	   
	   Particle picked =  pickParticle();
           next[i].pos = getNewPositionGauss(picked.pos,depth,speed);
	   next[i].weight = 0;
	   
    }
    
    replaceVector(next);
    
   // printAll();
    
    verifyPosition();
}



/**
* @brief If an APU answeres to the Ping of the verifyLocation method, the weight of the particles are adapted here
* 	 according to the acoustic model and the expected runtime of such an acoustic signal
*
* @param instance	
*/
void signalReciever(Acoustic_LocatingSignal* instance){
   if(strcmp(instance->receiver,apu_id)!=0) return;
    cout << "Recieved Acoustig Locating Signal of " << instance->sender << endl;
    
    double w = 0;
    
    //Testing
    PositionRequestWrite(apu_id,0);
    PositionRequestWrite(instance->sender,0);
   
    gazebo_RuntimeRequest* writeInstance = gazebo_RuntimeRequestTypeSupport::create_data();
    strcpy(writeInstance->apu_a,apu_id);
    strcpy(writeInstance->apu_b,instance->sender);
    
    RuntimeRequest_write(writeInstance);
        
    while(actTravelTime==0){}
    
    int id2;
    int n = sscanf(instance->sender,"apu-%d",&id2);
    
    
   
    Acoustic_ParticleSeq particleSeq = instance->particleSet;
   
    
    for(int j = 0; j < partSet.size; j++){
      w=0;
      for(int i = 0;i<instance->size;i++){
	 
	
	  Acoustic_Particle particle = particleSeq[i];
	  pos3d pos = newPosition(particle.x,particle.y,particle.z);
	  
	  
	  
	  double temp = getProbability(partSet.set[j].pos,pos,actTravelTime)*particle.w;
	  
	  w += temp;

      }
     partSet.set[j].weight += w*(4-(2*instance->error));
    
    }
    
    addPosition(getPairArray(particleSeq,instance->size),id2);
    addConnection(id2,actTravelTime);
    
    actTravelTime=0;
    setAnswered(instance->sender);
   
}

void pingHandler(Acoustic_Ping* instance){
    if(strcmp(instance->receiver,apu_id)!=0) return;
    cout << "Received Ping \n";
    Acoustic_LocatingSignal* signal_instance = Acoustic_LocatingSignalTypeSupport::create_data();
    
    signal_instance->sender = apu_id;
    signal_instance->receiver=instance->sender;
    
    signal_instance->size=partSet.set.size();
    signal_instance->particleSet.length(signal_instance->size);
    for(int i = 0; i<signal_instance->size; i++){
	signal_instance->particleSet[i].x=partSet.set[i].pos.x;
	signal_instance->particleSet[i].y=partSet.set[i].pos.y;
	signal_instance->particleSet[i].z=partSet.set[i].pos.z;
	signal_instance->particleSet[i].w=partSet.set[i].weight;
    }
    signal_instance->error = entropy();
      gettimeofday(&tp, 0);   
      signal_instance->timestamp = tp.tv_sec * 1000000 + tp.tv_usec;
      LocatingSignalWrite(signal_instance);
    
}

void localizationHandler(de_dlr_enex_datamodel_range_Localization* instance){
     int id2;
     int n = sscanf(instance->apu,"apu-%d",&id2);
   
      
    if(id<ApuPositions.size()){
	ApuPositions[id2].x = instance->relative_position.position.x;
	ApuPositions[id2].y = instance->relative_position.position.y;
	ApuPositions[id2].z = instance->relative_position.position.z;
	
    }
      if(id2!=id) {
	  cout << "\n Received Position update of: " << instance->apu << std::endl;	
	  cout << "X: " << ApuPositions[id2].x << " Y: " << ApuPositions[id2].y << " Z: " << ApuPositions[id2].z << endl;
      }
}

void runtimeHandler(gazebo_AcousticRuntime* instance){
    if(strcmp(instance->apu_a.id,apu_id)==0 || strcmp(instance->apu_b.id,apu_id)==0){
	if(actTravelTime==0){
	    actTravelTime=instance->delay;
	}
	else{
	    std::cout << "Tried to update travelTime, but it is still in use\n";
	}
    }
}


int main (int argc, char *argv[])
{
  
   
  LocalizationSubscriber_init(1,"gazebo_LocalizationAnswer",&localizationHandler);
	RuntimeRequestPublisher_init(1,"gazebo_RuntimeRequest");
	AcousticRuntimeSubscriber_init(1,"gazebo_AcousticRuntime",&runtimeHandler);
	PositionRequestPublisher_init(1,"gazebo_PositionRequest");
  ReturnSubscriber_init(1,"gazebo_MeltResponse",&ReturnHandler);
	PingPublisher_init(1,"AcousticPing");
	ConnectionPublisher_init(1,"Connection");
	LocatingSignalPublisher_init(1,"LocatingSignalSignal");
	LocatingSignalSubscriber_init(1,"LocatingSignalSignal",&signalReciever);
	//MeltRequest_init(1,"MeltingRequest",&relocationHandler);
	PingSubscriber_init(1,"AcousticPing",&pingHandler);
	
	

      	if(argc>=2) apu_id = argv[1];
	else apu_id = "apu-0";
	
	
	
	std::string filename(apu_id);
	initPlot(filename);
  
	double x=0;
	double y=0;
	double z=0;
	if(argc>=4) x = atoi(argv[3]);
	
	if(argc>=5) y = atoi(argv[4]);
	
	
	if(argc>=6) z = atoi(argv[5]);
	
  
	if(argc>=3) maxApu = atoi(argv[2]);
	else maxApu=12;
	
	ApuPositions.resize(maxApu);
	Answered.resize(maxApu);
	usleep(500000);
	PositionRequestWrite(apu_id,0);
	usleep(1000000);
	
	int n =sscanf(apu_id,"apu-%d",&id);
	
	initMappingInterface(id,maxApu);
	
	if(x==0 && y==0 && z==0){
	    if(ApuPositions[id].x==0 && ApuPositions[id].y==0 && ApuPositions[id].z==0){
		z = 10;
	    }
	    x = ApuPositions[id].x;
	    y = ApuPositions[id].y;
	    z = ApuPositions[id].z;
	}
	
	
	cout << "Started APU: " << apu_id << " at: " << x << " " << y << " " << z << endl;
	
      	
	
	initBoolVector(Answered);
      	
	unsigned int seed = time(NULL);
	srand(seed);
    
      	initMeltingModel(0.2,0.2,0.6);
      
	initParticleSet(100 ,newPosition(x,y,z));
	
	
	
       cout << "APU-Positionmodule Init finished\n";
	while(1);

}
