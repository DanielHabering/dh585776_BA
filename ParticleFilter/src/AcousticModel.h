#include <stdio.h>
#include <stdlib.h>
#include "Position.h"

double getProbability(const pos3d& posA,const pos3d& posB, double timediff);
int getMaxWaittime(pos3d posA, pos3d posB);