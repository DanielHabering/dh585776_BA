#include "Plotting.h"
#include <iostream>

FILE *datastream;
FILE *errorstream;
std::string dataname;
std::string errorname;

void initPlot(std::string name){

     datastream = fopen(name.c_str(),"w" );
     dataname=name;
     name.append("-error");
     errorstream = fopen(name.c_str(),"w");
     errorname=name;
     
    std::cout << "opnening File: " << name.c_str() << "\n";
   
}

void clearPlot(){
    
}

void addPoint(double x, double y, double z, double w){
  //  std::cout << "Adding point " << y << " " << x << " " << z << " " << w << "\n";
    double t = 255*(1-3*w)-100;
    //std::cout << "R und G: " << t << " " << t << "\n";
    fprintf(datastream, "%f %f %f %f \n",x,y,z,w);   
  
}

void addError(double error, int count){
    fprintf(errorstream, "%i %f\n",count,error);
}



void plot(int id, int i){
   fclose(datastream);
    FILE *plotstream;
    fflush(errorstream);
  if(i%5==0){
   
   plotstream=popen("gnuplot","w");
   fprintf(plotstream, "set terminal jpeg \n set output '%iplotX-%i.jpg'\n",id,i);
    fprintf(plotstream, "set xlabel \"X\" \n");
 fprintf(plotstream, "set ylabel \"Weight\" \n");   
    fprintf(plotstream, "set autoscale\n");
  //  fprintf(plotstream, "set style circle radius graph 0.02, first 0.00000, 0.00000 \n set style ellipse size graph 0.05, 0.03, first 0.00000 angle 0 units xy\n");
    fprintf(plotstream, "rgb(r,g,b) = 65536 * int(r) + 256 * int(g) + int(b)\n");
    fprintf(plotstream, "plot \"%s\" using 1:4 with points pointsize 2 pointtype 7\n",dataname.c_str());
    fprintf(plotstream, "set output\n");
    fclose(plotstream);	
    
    plotstream=popen("gnuplot","w");
     fprintf(plotstream, "set terminal jpeg \n set output '%iplotY-%i.jpg'\n",id,i);
    fprintf(plotstream, "set xlabel \"Y\" \n");
 fprintf(plotstream, "set ylabel \"Weight\" \n");   
    fprintf(plotstream, "set autoscale\n");
  //  fprintf(plotstream, "set style circle radius graph 0.02, first 0.00000, 0.00000 \n set style ellipse size graph 0.05, 0.03, first 0.00000 angle 0 units xy\n");
    fprintf(plotstream, "rgb(r,g,b) = 65536 * int(r) + 256 * int(g) + int(b)\n");
    fprintf(plotstream, "plot \"%s\" using 2:4 with points pointsize 2 pointtype 7 \n",dataname.c_str());
    fprintf(plotstream, "set output\n");
    fclose(plotstream);	
    
    plotstream=popen("gnuplot","w");
    fprintf(plotstream, "set terminal jpeg \n set output '%iplotZ-%i.jpg'\n",id,i);
    fprintf(plotstream, "set xlabel \"Z\" \n");
 fprintf(plotstream, "set ylabel \"Weight\" \n");   
    fprintf(plotstream, "set autoscale\n");
  //  fprintf(plotstream, "set style circle radius graph 0.02, first 0.00000, 0.00000 \n set style ellipse size graph 0.05, 0.03, first 0.00000 angle 0 units xy\n");
    fprintf(plotstream, "rgb(r,g,b) = 65536 * int(r) + 256 * int(g) + int(b)\n");
    fprintf(plotstream, "plot \"%s\" using 3:4 with points pointsize 2 pointtype 7\n",dataname.c_str());
    fprintf(plotstream, "set output\n");
    fclose(plotstream);	
  }
    plotstream=popen("gnuplot","w");
     fprintf(plotstream, "set terminal jpeg \nset output '%s.jpg'\n",errorname.c_str());
    fprintf(plotstream, "set xlabel \"Melting Step\" \n");
 fprintf(plotstream, "set ylabel \"Error\" \n");   
    fprintf(plotstream, "set autoscale\n");
  //  fprintf(plotstream, "set style circle radius graph 0.02, first 0.00000, 0.00000 \n set style ellipse size graph 0.05, 0.03, first 0.00000 angle 0 units xy\n");
    fprintf(plotstream, "rgb(r,g,b) = 65536 * int(r) + 256 * int(g) + int(b)\n");
    fprintf(plotstream, "plot \"%s\" using 1:2 with points pointsize 2 pointtype 7\n",errorname.c_str());
    fprintf(plotstream, "set output\n");
    fclose(plotstream);	
    
   
    
  /*  plotstream=popen("gnuplot","w");
    fprintf(plotstream, "set terminal jpeg \nset output \"plotyz%i-%i.jpg\"\n",id,i);
    fprintf(plotstream, "set xlabel \"Y\" \n");
 fprintf(plotstream, "set ylabel \"Z\" \n");   
    fprintf(plotstream, "set autoscale\n");
  //  fprintf(plotstream, "set style circle radius graph 0.02, first 0.00000, 0.00000 \n set style ellipse size graph 0.05, 0.03, first 0.00000 angle 0 units xy\n");
    fprintf(plotstream, "rgb(r,g,b) = 65536 * int(r) + 256 * int(g) + int(b)\n");
    fprintf(plotstream, "plot \"%s\" using 3:4:(rgb($4,$5,$6)) with points pointsize 2 pointtype 7 lc rgb variable\n",dataname.c_str());
    fprintf(plotstream, "set output\n");
    fclose(plotstream);	
    */
   datastream= fopen(dataname.c_str(),"w");
}
