#include <stdlib.h>
#include <stdio.h>
#include <string>


void initPlot(std::string name);

void addPoint(double x, double y, double z, double w);

void plot(int id, int i);

void addError(double error, int count);