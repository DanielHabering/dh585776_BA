#include "Position.h"
#include <iostream>
#include <math.h>
pos3d newPosition(double x, double y, double z){
   pos3d ret;
   ret.x = x;  
   ret.y = y;
   ret.z = z;

  return ret;

}

void print(pos3d pos)
{
	std::cout << " X: " << pos.x << " Y: " << pos.y << " Z: " << pos.z << std::endl ;
}


double getDistance(pos3d posA, pos3d posB){
    double ret = 0; 
    ret = (posA.x-posB.x)*(posA.x-posB.x);
    ret += (posA.y-posB.y)*(posA.y-posB.y);
    ret += (posA.z-posB.z)*(posA.z-posB.z);
    ret = sqrt(ret);
    
    return ret;
}