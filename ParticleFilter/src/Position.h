#include <stdlib.h>
#include <stdio.h>


#ifndef POS_H
#define POS_H
typedef struct pos3d{
   double x;
   double y;
   double z;
   
   bool operator<( const pos3d & n ) const {
	  return this->x < n.x;   // for example
	}
	
   pos3d(double x, double y, double z){
      this->x = x; 
      this->y = y;
      this->z = z;
   };
   pos3d(){};
	
}pos3d;

void print(pos3d pos);

double getDistance(pos3d posA, pos3d posB);

pos3d newPosition(double x, double y, double z);
#endif
