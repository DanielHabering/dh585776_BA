#include "MeltingModel.h"
#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <time.h>
#include <ctime>
#include <random>	


double varianzX,varianzY,varianzZ;
boost::mt19937 rng;

void initMeltingModel(double VarianzX, double VarianzY, double VarianzZ){
    varianzX=VarianzX;
    varianzY=VarianzY;
    varianzZ=VarianzZ;
    
	
	
}

pos3d getNewPositionUniform(const pos3d& oldPos, double depth, double speed){
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distributionX(-0.25,0.25);
  
  std::uniform_real_distribution<double> distributionZ(-0.5,0.5);
  
  pos3d ret;
  ret.x = oldPos.x+distributionX(generator);
  ret.y = oldPos.y+distributionX(generator);
  ret.z = oldPos.z-depth+distributionZ(generator);
  
  return ret;
  
}

pos3d getNewPositionGauss(const pos3d& oldPos, double depth, double speed){
	pos3d ret;
	static unsigned int seed = 0;
	rng.seed((++seed) + time(NULL));
	boost::normal_distribution<> zd(oldPos.z-depth,varianzZ);
	boost::normal_distribution<> xd(oldPos.x,varianzX);
	boost::normal_distribution<> yd(oldPos.y,varianzY);
	
	boost::variate_generator<boost::mt19937&,boost::normal_distribution<> > var_z(rng,zd);
	boost::variate_generator<boost::mt19937&,boost::normal_distribution<> > var_x(rng,xd);
	boost::variate_generator<boost::mt19937&,boost::normal_distribution<> > var_y(rng,yd);
	
	ret.x=var_x();
	ret.y=var_y();
	ret.z=var_z();
	

	return ret;
}
