#include "AcousticModel.h"
#include "Position.h"
#include <math.h>
#include <iostream>
#include <cmath>
#define MIN_ACOUSTIC_SPEED 1400

double varianz = 12.5;
double oldDist = 0;
double getProbability(const pos3d& posA,const pos3d& posB, double timediff){
	double ret = 0;
	double dist = getDistance(posA,posB);
	double time = (dist*100000)/325;
	
	ret = -1*(pow(((time-timediff)),2))/(2*pow(varianz,2));
	ret = exp(ret);
	ret = ret/(varianz*2*M_PI);

	//std::cout << "";
	if(oldDist!=time){
	  oldDist=time;
	// std::cout << "{" << ret << ",";
	}
	return ret;
}
int getMaxWaittime(pos3d posA, pos3d posB){
  double dist = getDistance(posA, posB);
  
  int ret = 2.5*dist*MIN_ACOUSTIC_SPEED*1000;
  return ret;
}

