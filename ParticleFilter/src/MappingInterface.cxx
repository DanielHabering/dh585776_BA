#include "MappingInterface.h"
#include <iostream>

std::vector<pos3d> apuPos;
std::vector<int> Connection;
std::vector<long int> travelTime;
int parentId;

void clear(){
    Connection.clear();
    Connection.resize(travelTime.size());
    travelTime.clear();
    travelTime.resize(Connection.size());
}

void addConnection(int id2, long int time)
{
    if(id2<0 || id2 >= Connection.size()){
	std::cout << "Index out of bound\n";
	return;
    }
    Connection[id2]=0;
    travelTime[id2]=time;
}

void addInterrupt(int id2)
{
    if(id2<0 || id2 >= Connection.size()){
	std::cout << "Index out of bound\n";
	return;
    }
    Connection[id2]=1;
}

void addPosition(const std::vector<std::pair<pos3d,double>> &particleSet, int id)
{
     pos3d bar = newPosition(0,0,0);
      double total = 0;

      for(int i = 0; i< particleSet.size(); i++){
	bar.x+=particleSet[i].first.x*particleSet[i].second;
	bar.y+=particleSet[i].first.y*particleSet[i].second;
	bar.z+=particleSet[i].first.z*particleSet[i].second;
	
    }
    
    
    apuPos[id]=bar;
}

void updateMap()
{	std::cout << "\n Updating Map \n";
      for(int i = 0; i < Connection.size(); ++i){
	if(i==parentId) continue;
	
	 Mapping_Connection *instance = Mapping_ConnectionTypeSupport::create_data();
	   instance->apuA.x = apuPos[i].x;
	   instance->apuA.y = apuPos[i].y;
	   instance->apuA.z = apuPos[i].z;
	   
	   instance->apuB.x = apuPos[parentId].x;
	   instance->apuB.y = apuPos[parentId].y;
	   instance->apuB.z = apuPos[parentId].z;
	if(Connection[i]==1){
	 
	   
	   instance->qual = NO_CONNECTION; 
	   
	}
	else{
	    if(Connection[i]==0){
		double dist = getDistance(apuPos[i],apuPos[parentId]);
		
		/*
		 * 	Akustik Modell einbauen
		 */
		
		long long int refTime = dist*1000000/3250;  
		if(abs(travelTime[i]-refTime)<0.1*refTime){
		  instance->qual=CONNECTION;
		}
		else{
		   instance->qual=WEAK_CONNECTION;
		}
	    }
	}
	std::cout << instance->apuA.x << " " << instance->apuA.y << " " << instance->apuA.z << " -> " << instance->apuB.x << " " << instance->apuB.y << " " << instance->apuB.z << " ---- " << instance->qual << std::endl;
	Connection_write(instance);
      }
      
      clear();
}

void initMappingInterface(int id, int max){
    parentId = id;
    ConnectionPublisher_init(1,"Connection");
    std::vector<pos3d> p(max);
    p.resize(max);
    apuPos.resize(max);
    
    
   Connection.resize(max);
    
    travelTime.resize(max);
}