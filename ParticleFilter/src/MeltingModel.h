#include <stdio.h>
#include <stdlib.h>
#include "Position.h"

pos3d getNewPositionGauss(const pos3d& oldPos, double depth, double speed);

pos3d getNewPositionUniform(const pos3d& oldPos, double depth, double speed);

void initMeltingModel(double VarianzX, double VarianzY, double VarianzZ);