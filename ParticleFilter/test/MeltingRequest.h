

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from MeltingRequest.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef MeltingRequest_1266771341_h
#define MeltingRequest_1266771341_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern "C" {

    extern const char *Melting_MeltRequestTYPENAME;

}

struct Melting_MeltRequestSeq;
#ifndef NDDS_STANDALONE_TYPE
class Melting_MeltRequestTypeSupport;
class Melting_MeltRequestDataWriter;
class Melting_MeltRequestDataReader;
#endif

class Melting_MeltRequest 
{
  public:
    typedef struct Melting_MeltRequestSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef Melting_MeltRequestTypeSupport TypeSupport;
    typedef Melting_MeltRequestDataWriter DataWriter;
    typedef Melting_MeltRequestDataReader DataReader;
    #endif

    DDS_Char *   id ;
    DDS_Double   depth ;
    DDS_Double   speed ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Melting_MeltRequest_get_typecode(void); /* Type code */

DDS_SEQUENCE(Melting_MeltRequestSeq, Melting_MeltRequest);                                        

NDDSUSERDllExport
RTIBool Melting_MeltRequest_initialize(
    Melting_MeltRequest* self);

NDDSUSERDllExport
RTIBool Melting_MeltRequest_initialize_ex(
    Melting_MeltRequest* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Melting_MeltRequest_initialize_w_params(
    Melting_MeltRequest* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void Melting_MeltRequest_finalize(
    Melting_MeltRequest* self);

NDDSUSERDllExport
void Melting_MeltRequest_finalize_ex(
    Melting_MeltRequest* self,RTIBool deletePointers);

NDDSUSERDllExport
void Melting_MeltRequest_finalize_w_params(
    Melting_MeltRequest* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Melting_MeltRequest_finalize_optional_members(
    Melting_MeltRequest* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Melting_MeltRequest_copy(
    Melting_MeltRequest* dst,
    const Melting_MeltRequest* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* MeltingRequest */

